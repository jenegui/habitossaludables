$(document).ready(function() {
	
	$( "#div_pre1" ).show();
	$( "#div_pre2" ).hide();
	$( "#div_pre3" ).hide();
	$( "#div_pre4" ).hide();
	$( "#div_pre5" ).hide();
	$( "#msj_pre1_1" ).hide();
	$( "#msj_pre1_0" ).hide();
    $( "#msj_pre2_1" ).hide();
	$( "#msj_pre2_0" ).hide();
    $( "#msj_pre3_1" ).hide();
	$( "#msj_pre3_0" ).hide();
    $( "#msj_pre4_1" ).hide();
	$( "#msj_pre4_0" ).hide();
    $( "#msj_pre5_1" ).hide();
	$( "#msj_pre5_0" ).hide();
	$( "#div_conf" ).hide();
    $("#div_ira1").hide();
    $("#div_ira2").hide();
    $("#div_ira3").hide();
    $("#div_ira4").hide();
    $("#div_ira5").hide();
    $("#div_ira6").hide();
	
	$("#correo_conf").on('paste', function(e){
		e.preventDefault();
		alertify.error('Esta acción está prohibida');
	})
  
	$("#correo_conf").on('copy', function(e){
		e.preventDefault();
		alertify.error('Esta acción está prohibida');
	})

	var $signupForm1 = $( '#formContacto' );
    var $signupForm2 = $( '#formContactoInd' );
    var $signupForm3 = $( '#formContactoCol' );
    var $signupForm4 = $( '#formSeguimiento' );

    $signupForm1.validationEngine({
        promptPosition : "topRight",
        scroll: false,
        autoHidePrompt: true,
        autoHideDelay: 2000
    });

    $signupForm1.submit(function() {
        var $resultado=$signupForm1.validationEngine("validate");

        if ($resultado) {
			return true;
        }
        return false;
    });

     $signupForm2.validationEngine({
        promptPosition : "topRight",
        scroll: false,
        autoHidePrompt: true,
        autoHideDelay: 2000
    });

    $signupForm2.submit(function() {
        var $resultado=$signupForm2.validationEngine("validate");

        if ($resultado) {
            return true;
        }
        return false;
    });

     $signupForm3.validationEngine({
        promptPosition : "topRight",
        scroll: false,
        autoHidePrompt: true,
        autoHideDelay: 2000
    });

    $signupForm3.submit(function() {
        var $resultado=$signupForm3.validationEngine("validate");

        if ($resultado) {
            return true;
        }
        return false;
    });

    $signupForm4.validationEngine({
        promptPosition : "topRight",
        scroll: false,
        autoHidePrompt: true,
        autoHideDelay: 2000
    });

    $signupForm4.submit(function() {
        var $resultado=$signupForm4.validationEngine("validate");

        if ($resultado) {
            return true;
        }
        return false;
    });

    $("input[name=pregunta1]:radio").change(function () {
        
        $("input[name='pregunta2']:radio").prop('checked',false);
        $("input[name='pregunta3[]']:checkbox").prop('checked',false);
        $("#msj_pre1_0").hide();
        $("#msj_pre2_0").hide();     
        $("#msj_pre3_1").hide();     
        $("#msj_pre3_0").hide();
        
        if($(this).val() == 1){           
			$("#div_pre2").show();	
			//$("#msj_pre1_0").hide(); 
            $( "#div_conf" ).show();
        }else{
			$("#div_pre2").show();	
            //$("#msj_pre1_0").hide();
			$( "#div_conf" ).show();			
        }
        
    });
	
	$("input[name=pregunta2]:radio").change(function () {
        
        $( "#div_conf" ).show();
        var pre1 = $('input[name="pregunta1"]:checked').val();
        
        if($('input[name="pregunta2"]:checked').val() == 1){           
        //if($(this).val() == 1){           
			$("input[name='pregunta3[]']:checkbox").attr('disabled',false);
            
            if(pre1 == 1){
                //1=SI y 2=SI
                $("#msj_pre3_1").hide();     
                $("#msj_pre3_0").hide();     
                $("#msj_pre2_0").hide();
                $("#msj_pre1_0").hide();
            }else{
                //1=NO y 2=SI
                $("#msj_pre3_0").hide(); 
                $("#msj_pre3_1").hide(); 
                $("#msj_pre2_0").hide(); 
                $("#msj_pre1_0").hide();
            }			
        }else{
            
            $("input[name='pregunta3[]']:checkbox").attr('disabled',true);
			$("input[name='pregunta3[]']:checkbox").prop('checked',false);
            
            if(pre1 == 1){
                //1=SI y 2=NO
                $("#msj_pre1_0").hide();
                $("#msj_pre2_0").hide();     
                $("#msj_pre3_1").hide();     
                $("#msj_pre3_0").show();  
            }else{
                //1=NO y 2=NO
                $("#msj_pre1_0").show(); 
                $("#msj_pre2_0").hide(); 
                $("#msj_pre3_0").hide(); 
                $("#msj_pre3_1").hide(); 
            }            
        }
        
    });
        
    
    $("input[name='pregunta3[]']:checkbox").change(function () {
        
        var mesesUsuario = calcularEdad($("#enc_fech_nac").val(), 'meses', 'NO');
        $("#edad").val(mesesUsuario);
        
		var totalcheck = $('input[name="pregunta3[]"]:checked').length;
		
		if(totalcheck >= 1){
			
			$("#msj_pre3_0").hide();
			$("#msj_pre3_1").show();
			$("#div_pre3").show();	
			
		}else{
			
			$("#msj_pre3_0").show();
			$("#msj_pre3_1").hide();
			$("#div_pre3").hide();
			$( "#div_conf" ).show();
		}
        
        if($('input[name="pregunta1"]:checked').val() == 0 && $('input[name="pregunta2"]:checked').val() == 1){
            
            $("#msj_pre2_0").hide();  
            $("#msj_pre3_0").hide();  
            $("#msj_pre3_1").hide();  
            $("#msj_pre1_0").hide(); 
            
            if($("#enc_fech_nac").val() != ''){
                
                var mesesUsuario = calcularEdad($("#enc_fech_nac").val(), 'meses', 'NO');
                $("#edad").val(mesesUsuario);
                
                $("#div_ira1").hide();
                $("#div_ira2").hide();
                $("#div_ira3").hide();
                $("#div_ira4").hide();
                $("#div_ira5").hide();
                $("#div_ira6").hide();

                var sintomas = [];

                $.each($("input[name='pregunta3[]']:checked"), function(){

                    if($(this).val() == 1){
                        $("#div_ira1").show();
                    }else if($(this).val() == 2){
                        $("#div_ira2").show();
                    }else if($(this).val() == 3){
                        $("#div_ira3").show();
                    }else if($(this).val() == 4){

                    }else if($(this).val() == 5){

                    }else if($(this).val() == 6){
						$("#div_ira4").show();
						$("#div_ira5").show();
						$("#div_ira6").show();
                    }

                });

                 
            }else{
                $("input[name='pregunta3[]']:checkbox").prop('checked',false);
                alertify.error("Primero debe agregar fecha de nacimiento");
            }
        }
    });
    
    
    /*
    $("input[name=pregunta4]:radio").change(function () {
        
        if($(this).val() == 1){
            $("#msj_pre4_1").show();
            $("#msj_pre4_0").hide();
			$( "#div_conf" ).show();			
        }else{
            $("#msj_pre4_0").show();
            $("#msj_pre4_1").hide();  
			$( "#div_conf" ).show();
        }
        
    });
    */
    
});

function validarSintoma(id, sintoma, valor){
    
    var id_html = id;
    var id_sintoma = sintoma;
    var sig_sintoma = parseInt(sintoma) + 1;
    var respuesta = valor;
    var edad = $("#edad").val();
    
    if(valor == 1){
        $.post( base_url + "covid/validarSintoma", {
            id_html: id_html, 
            id_sintoma: id_sintoma, 
            respuesta: respuesta, 
            edad: edad 
        },
        function(data){
            $("#div_resultado_" + id_sintoma).html(data);
			$("#div_preg_ira_" + sig_sintoma).addClass('d-none');
        });
    }else{
        $("#div_resultado_" + id_sintoma).html('');
        $("#div_preg_ira_" + sig_sintoma).removeClass('d-none');
    }
    
    
    
}

/**

 * Esta función calcula la edad de una persona y los meses

 * La fecha la tiene que tener el formato yyyy-mm-dd que es

 * metodo que por defecto lo devuelve el <input type="date">

 */

function calcularEdad(edadUsuario, parametro = '', mensaje = '')
{
    var fecha = edadUsuario;

    if(validate_fecha(fecha)==true)
    {

        // Si la fecha es correcta, calculamos la edad
        var values=fecha.split("-");
        var dia = values[2];
        var mes = values[1];
        var ano = values[0];

        // cogemos los valores actuales
        var fecha_hoy = new Date();
        var ahora_ano = fecha_hoy.getYear();
        var ahora_mes = fecha_hoy.getMonth()+1;
        var ahora_dia = fecha_hoy.getDate();

        // realizamos el calculo
        var edad = (ahora_ano + 1900) - ano;

        if ( ahora_mes < mes )
        {
            edad--;
        }

        if ((mes == ahora_mes) && (ahora_dia < dia))
        {
            edad--;
        }

        if (edad > 1900)
        {
            edad -= 1900;
        }

        // calculamos los meses
        var meses=0;

        if(ahora_mes>mes)
            meses=ahora_mes-mes;

        if(ahora_mes<mes)
            meses=12-(mes-ahora_mes);

        if(ahora_mes==mes && dia>ahora_dia)
            meses=11;

        // calculamos los dias
        var dias=0;

        if(ahora_dia>dia)
            dias=ahora_dia-dia;

        if(ahora_dia<dia)
        {
            ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
            dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
        }

        if(mensaje == 'SI'){
            alertify.success("Edad: "+edad+" años, "+meses+" meses y "+dias+" días");
        }

        if(parametro == 'meses'){
            totalEdad = edad * 12;
            totalMeses = totalEdad + meses;

            return totalMeses;
        }else if(parametro == 'anios'){
            return edad;
        }

    }else{
        alertify.error("La fecha "+fecha+" es incorrecta");
    }

}


function isValidDate(day,month,year)
{
    var dteDate;

    // En javascript, el mes empieza en la posicion 0 y termina en la 11

    //   siendo 0 el mes de enero

    // Por esta razon, tenemos que restar 1 al mes

    month=month-1;

    // Establecemos un objeto Data con los valore recibidos

    // Los parametros son: año, mes, dia, hora, minuto y segundos

    // getDate(); devuelve el dia como un entero entre 1 y 31

    // getDay(); devuelve un num del 0 al 6 indicando siel dia es lunes,

    //   martes, miercoles ...

    // getHours(); Devuelve la hora

    // getMinutes(); Devuelve los minutos

    // getMonth(); devuelve el mes como un numero de 0 a 11

    // getTime(); Devuelve el tiempo transcurrido en milisegundos desde el 1

    //   de enero de 1970 hasta el momento definido en el objeto date

    // setTime(); Establece una fecha pasandole en milisegundos el valor de esta.

    // getYear(); devuelve el año

    // getFullYear(); devuelve el año

    dteDate=new Date(year,month,day);

    //Devuelva true o false...
    return ((day==dteDate.getDate()) && (month==dteDate.getMonth()) && (year==dteDate.getFullYear()));

}

function validate_fecha(fecha)
{
    var patron=new RegExp("^(19|20)+([0-9]{2})([-])([0-9]{1,2})([-])([0-9]{1,2})$");

    if(fecha.search(patron)==0)
    {
        var values=fecha.split("-");

        if(isValidDate(values[2],values[1],values[0]))
        {
            return true;
        }
    }

    return false;

}

$( function() {
    $( "#tabs" ).tabs();
    
  } );



