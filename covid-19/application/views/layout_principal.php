<!DOCTYPE html>
<html lang="es">
<head>
    <!--Meta tags-->
    <meta charset="UTF-8">
	<meta name="keywords" content="">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1">
    <meta http-equiv="content-language" content="es">
    <meta name="distribution" content="global">
    <meta name="robots" content="all">
    <!--favicon-->
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/images/favicon/favicon.png')?>"/>
    <!--Titulo del sistema de información-->
    <title>
        <?php echo $titulo?>
    </title>
	
	<!--
		CSS
	-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
	<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:400,800" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/styles_rx.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatable.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/select2/select2.css')?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/validationEngine.jquery.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-ui.css')?>">
    <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->
    
  
	<!--
		JS
	-->
	<script type="text/javascript">
        var base_url = "<?php echo base_url(); ?>";
    </script>

    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.2.1.js')?>"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validationEngine.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validationEngine-es.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/select2/select2.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/datatable.js')?>"></script>
	<!-- JavaScript -->
    <script src="<?php echo base_url('assets/js/alertifyjs/alertify.min.js') ?>"></script> 
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/js/alertifyjs/css/alertify.min.css') ?>" />
    <!-- Default theme -->
    <link rel="stylesheet" href="<?php echo base_url('assets/js/alertifyjs/css/themes/default.min.css') ?>" />
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="<?php echo base_url('assets/js/alertifyjs/css/themes/semantic.min.css') ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/js/jquery-ui.js') ?>" />
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <?php
        if(isset($js) && count($js) > 0){
            for($i=0;$i<count($js);$i++){
                echo '<script type="text/javascript" src="'.$js[$i].'"></script>';
            }
        }

    ?>

    <?php
    if($controller=='covid'){
	?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159823457-1"></script>
        <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'UA-159823457-1');
        </script>
    <?php
    }else{
    ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160638542-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-160638542-1');
        </script>
    <?php
    }
    ?>
	

</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#"><img src="<?php echo base_url('assets/imgs/covid19/alcaldiac_logo.png')?>"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo base_url()?>">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('covid19/documento/1')?>">ABC Coronavirus</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('covid19/documento/2')?>">Vigilancia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('covid19/documento/3')?>">Acciones</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('covid19/documento/4')?>">Circular 005</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('covid19/documento/5')?>">Circular 0017</a>
                    </li>-->
                </ul>
            </div>
    </nav>

	<div class="container-lg">
	<?php
		$this->load->view($contenido);
	?>
	</div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4"></div>
                <div class="col-12 col-md-4 text-md-center">
                    <p>2020. @ Todos los derechos reservados- Versión 1.0</p>
                    <p><a target="_blank" href="http://www.saludcapital.gov.co/Documents/Politica_Proteccion_Datos_P.pdf">*Políticas de protección y tratamiento de datos personales</a></p>
                    <p><a href="http://www.saludcapital.gov.co/Documents/Politicas_Sitios_Web.pdf" target="_blank">*Políticas de privacidad y términos de uso del sitio web</a></p>
                </div>
                <div class="col-12 col-md-4 text-md-right">
                    <img src="<?php echo base_url('assets/imgs/covid19/logoalcaldia.png')?>" alt="">
                </div>
            </div>
        </div>
    </footer>    
</body>

</html>
