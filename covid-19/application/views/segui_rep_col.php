<div class="row">    
    <div class="col">
        <div class="card">
            <div class="card-header">
                
            </div>
            <div class="card-body">
               
                	 <div class="row">
                        <div class="col">
                        	
                            Seguimiento al reporte de autocuidado:
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="nombre">Nombre:</label>
                            <label class="form-check-label" for="inlineRadio1"><?php echo $nombre; ?></label>
                        </div>    
                        <div class="col">
                            <label for="nombre">Identificaci&oacute;:</label>
                            <label class="form-check-label" for="inlineRadio1"><?php echo $identificacion; ?></label>
                        </div>
                    </div>
                   	<div class="row" id="div_preg1">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Como empresas y espacios laborales organizan trabajo en casa de todos los empleados que le sea posible? </label><br>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label" for="inlineRadio1"><?php echo $pregunta1==1?'SI':'NO'; ?></label>
                            </div>
                        </div>                   
                    </div>
                    <div class="row" id="div_preg2">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Para los empleados que sea indispensable que asistan al lugar de trabajo, se organizan al menos 3 turnos de entrada y salida a lo largo del día laboral? </label><br>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label" for="inlineRadio1"><?php echo $pregunta2==1?'SI':'NO'; ?></label>
                            </div>
                        </div>                   
                    </div>
                    <div class="row" id="div_preg3">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Además del trabajo en casa y turnos de ingreso y salida, las universidades y colegios organizan la virtualización de tantas clases y actividades como les sea posible? </label><br>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label" for="inlineRadio1"><?php echo $pregunta3==1?'SI':'NO'; ?></label>
                            </div>
                        </div>                   
                    </div>
                    <div class="row" id="div_preg4">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Aplaza todo tipo de evento público o privado de concentración masiva, de más de mil personal, en contacto estrecho, es decir a menos de 2 metros por más de 15 minutos? </label><br>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label" for="inlineRadio1"><?php echo $pregunta4==1?'SI':'NO'; ?></label>
                            </div>
                        </div>                   
                    </div>
                    <div class="row" id="div_enviar">
                        <hr>
                        <div class="text-center">
                            <a class="btn btn-success" href=" <?php echo base_url(). 'habitos/'; ?> "><span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Regresar </a> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>