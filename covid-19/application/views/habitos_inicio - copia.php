<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
    <div class="alert alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
        <?php echo $retornoExito ?>
    </div>
    <?php
}
    
?>
<div class="row">
	<div class="alert alert-info" role="alert">
		<p>
			Recuerde que la información aportada puede estar sometida a cambios dinámicos basados en los procesos de investigación reportado por la OPS y OMS y los lineamientos establecidos por el Ministerio de Salud y el Instituto Nacional de Salud. Para más información diríjase a las siguientes fuentes oficiales
		</p>
		<ul>
			<li>Ministerio Nacional de Salud:  
				<ul>
					<li><a href="https://www.minsalud.gov.co/sites/rid/Lists/BibliotecaDigital/RIDE/VS/preguntas-rtas-coronavirus.pdf" target="_blank">Preguntas frecuentes</li></li>
					<li><a href="https://www.minsalud.gov.co/sites/rid/Lists/BibliotecaDigital/RIDE/VS/PP/ET/abece-coronavirus.pdf" target="_blank">Abecé</li></li>
					<li><a href="https://www.minsalud.gov.co/salud/publica/PET/Paginas/Nuevo-Coronavirus-nCoV.aspx" target="_blank">Campañas</li></li>
					<li><a href="https://www.minsalud.gov.co/salud/publica/PET/Paginas/Nuevo-Coronavirus-nCoV.aspx" target="_blank">Videos</li></li> 
				</ul> 
			<li>Instituto Nacional de Salud: <a href="https://www.ins.gov.co/Noticias/Coronavirus/abece-coronavirus.pdf" target="_blank">Abecé - Coronavirus</a></li>
			<li><a href="http://www.saludcapital.gov.co/Paginas2/Coronavirus.aspx" target="_blank">Secretaria Distrital de Salud</a></li>
		</ul>
	</div>
</div>
<div class="row">
	<div class="col">
		<div id="carouselExampleControls" class="carousel carousel-with-lb slide " data-ride="carousel">
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img src="<?php echo base_url('assets/imgs/habitos/img1.jpg')?>" class="d-block w-100">
				</div>
				<div class="carousel-item">
					<img src="<?php echo base_url('assets/imgs/habitos/img2.jpg')?>" class="d-block w-100">
				</div>
				<div class="carousel-item">
					<img src="<?php echo base_url('assets/imgs/habitos/img3.jpg')?>" class="d-block w-100" alt="...">
				</div>
			</div>
			<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previa</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Siguiente</span>
			</a>
		</div>
	</div>	
</div>
<div class="row">    
    <div class="col">
        <div class="card">
            <div class="card-header">
                Flujograma de atención
            </div>
            <div class="card-body">
                <form class="form" name="formContacto" id="formContacto" method="post" action="<?php echo base_url('covid/procesarForm')?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col">
                            <label for="nombre">Número de identificación</label>
                            <input type="text" id="identificacion" name="identificacion" class="form-control validate[required, minSize[3], custom[onlyNumberSp]]">
                        </div>    
                        <div class="col">
                            <label for="nombre">Nombre</label>
                            <input type="text" id="nombre" name="nombre" class="form-control validate[required, minSize[5], custom[onlyLetterSp]]">
                        </div>
                    </div>
                    <div class="row">                            
                        <div class="col">
                            <label for="nombre">Correo Electrónico</label>
                            <input type="text" id="correo" name="correo" class="form-control validate[required, custom[email]]">
                        </div>
                        <div class="col">
                            <label for="nombre">Teléfono</label>
                            <input type="text" id="telefono" name="telefono" class="form-control validate[required, maxSize[10], minSize[7], custom[onlyNumberSp]]">
                        </div> 
                    </div>
                    <div class="row">                           
                        <div class="col">
                            <label for="nombre">Dirección</label>
                            <input type="text" id="direccion" name="direccion" class="form-control validate[required, minSize[5]]">
                        </div>
                        <div class="col">
                            <label for="nombre">EPS</label>
                            <input type="text" id="eps" name="eps" class="form-control validate[required, minSize[5], custom[onlyLetterSp]]">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="enc_fech_nac">Fecha de Nacimiento</label>
                            <input type="date" class="form-control validate[required]" id="enc_fech_nac" name="enc_fech_nac" placeholder="AAAA-MM-DD"> 
                            <input type="hidden" id="edad" name="edad">
                        </div>
                    </div>
                    <div class="row" id="div_pre1">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Viajó en los últimos 14 días a áreas con circulación de casos de enfermedad por coronavirus COVID-19 (China, Irán, Corea del sur, Japón, Italia, Francia, Alemania, España, Estados Unidos, Ecuador) o ha estado en contacto con una persona diagnosticada con coronavirus COVID-19? </label><br>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta1" id="pregunta1" value="1">
                              <label class="form-check-label" for="inlineRadio1">Si</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta1" id="pregunta1" value="0">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>							
                        </div>                   
                    </div>
                    <div class="row" id="div_pre2">
                        <div class="col-md-12">
							<hr>
                            <label for="nombre">¿Tiene alguno de los siguientes síntomas?</label><br>
							<div class="form-check form-check-inline">
								<input class="form-check-input validate[required]" type="radio" name="pregunta2" id="pregunta2" value="1">
								<label class="form-check-label" for="inlineRadio1">Si</label>
                            </div>
                            <div class="form-check form-check-inline">
								<input class="form-check-input validate[required]" type="radio" name="pregunta2" id="pregunta2" value="0">
								<label class="form-check-label" for="inlineRadio2">No</label>
                            </div>
							<div class="form-check form-check">
								<input class="form-check-input validate[required]" type="checkbox" name="pregunta3[]" id="pregunta3" value="1" disabled>
								<label class="form-check-label" for="inlineCheckbox1">Fiebre cuantificada mayor o igual a 38°C</label>
							</div>
							<div class="form-check form-check">
								<input class="form-check-input validate[required]" type="checkbox" name="pregunta3[]" id="pregunta3" value="2" disabled>
								<label class="form-check-label" for="inlineCheckbox2">Tos</label>
							</div>
							<div class="form-check form-check">
								<input class="form-check-input validate[required]" type="checkbox" name="pregunta3[]" id="pregunta3" value="3" disabled>
								<label class="form-check-label" for="inlineCheckbox3">Dificultad para respirar</label>
							</div>
                            <div class="form-check form-check">
								<input class="form-check-input validate[required]" type="checkbox" name="pregunta3[]" id="pregunta3" value="4" disabled>
								<label class="form-check-label" for="inlineCheckbox2">Dolor de garganta (Odinofagia) </label>
                            </div>
                            <div class="form-check form-check">
								<input class="form-check-input validate[required]" type="checkbox" name="pregunta3[]" id="pregunta3" value="5" disabled>
								<label class="form-check-label" for="inlineCheckbox2">Fatiga/decaimiento o debilidad</label>
                            </div>
                            <div class="form-check form-check">
								<input class="form-check-input validate[required]" type="checkbox" name="pregunta3[]" id="pregunta3" value="6" disabled>
								<label class="form-check-label" for="inlineCheckbox2">Otros (comportamiento o actitud, Alimentacion) </label>
                            </div>							
                        </div>
                        <div class="col-md-12">
                            <div class="alert alert-success" role="alert" id="msj_pre3_1">
                                <p>
									Usted puede tener una posible infección por el coronavirus COVID-19. Lávese las manos frecuentemente, cubra boca y nariz con tapabocas, evite contacto con otras personas, comuníquese con su aseguradora o IPS de atención primaria para su atención inmediata evitando ir a lugares públicos antes de la atención.  Informe si toma acetaminofén o ibuprofeno																			
								</p>
								<p>
									Si presenta alguno de estos síntomas consulte inmediamente a un servicio de urgencias.
								</p>
								<p>
									Puede comunicarse a la línea 3649666 para mayor información
								</p>
                            </div>
                            <div class="alert alert-success" role="alert" id="msj_pre3_0">
								<p>
									Usted puede continuar en riesgo deber asegurar aislamiento en casa por 14 días (debe estar en el protocolo de atención del puerto de arribo), debe consultar en forma inmediata a su IPS cita prioritaria o urgencias si presenta 2 o más de los siguientes síntomas
									<ul>
										<li>Fiebre cuantificada mayor o igual a 38°C</li> 
										<li>Tos</li> 
										<li>Dificultad para respirar</li>
										<li>Dolor de garganta Odinofagia)</li>
										<li>Fatiga/decaimiento o debilidad</li>
									</ul>
								</p>
                            </div>
							<div class="alert alert-success" role="alert" id="msj_pre2_0">
								<p>
									Posiblemente tiene una infección respiratoria aguda por gérmenes comunes, lave frecuentemente sus manos, use tapabocas de forma permanente, tosa o estornude sobre el codo, mantenga asilamiento en casa los primeros días de la enfermedad, ingiera abundantes líquidos.
								</p>
								<p>
									Si persiste con los síntomas o presenta fiebre más de 3 días o  de difícil control solicite una cita prioritaria a su IPS
								</p>
								<p>
									Si presenta síntomas que sugieran empeoramiento o gravedad como: fatiga, dificultad para respirar, dolor en el pecho, consulte inmediamente a un servicio de urgencias de su EPS.
								</p>
                            </div>
                            <div class="alert alert-success" role="alert" id="msj_pre1_0">
                               <p>
									No tiene riesgo para infección por coronavirus. Aplique medidas preventivas para evitar una infección respiratoria
									<ul>  
										<li><a href="http://www.saludcapital.gov.co/Documents/Videos/Video_Lavado_de_manos.mp4" target="_blank">Lavado de manos </a></li>
										<li><a href="http://www.saludcapital.gov.co/Paginas2/Coronavirus-graficas.aspx" target="_blank">Uso de tapabocas en caso de presentar síntomas respiratorios</a></li>
									</ul>
									Manténgase en buenas condiciones de salud: 
									<ul>
										<li>Asista a sus controles médicos</li>
										<li>Realice actividad física</li>
										<li>Asegure una alimentación balanceada</li>
										<li>Mantenga el esquema de vacunas al día </li>
										<li>Cuide su salud mental</li>
										<li>Evite exponer a los menores de 5 años a personas con síntomas respiratorios</li>
									</ul>
							   </p>
                            </div>
                        </div> 
                    </div>                        
                    <!--<div class="row" id="div_pre3">
                        <div class="col-md-12">
							<hr>
                            <label for="nombre">¿Cuenta con EPS o seguro viajero?</label><br>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta4" id="pregunta4" value="1">
                              <label class="form-check-label" for="inlineRadio1">Si</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta4" id="pregunta4" value="0">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="alert alert-success" role="alert" id="msj_pre4_1">
                                Comuníquese con su IPS primaria asignada por la EPS o su seguro viajero
                            </div>
							<div class="alert alert-danger" role="alert" id="msj_pre4_0">
                                Acudir al punto más cercano de las siguientes unidades de servicio
								<p>
									Atención exclusiva <b>POBLACIÓN PEDIÁTRICA</b>
									<ul>
										<li>USS. Patio Bonito el Tintal</li>
										<li>USS. Meissen</li>
									</ul>
								</p>
								<p>
									Atención <b>ADULTOS Y NIÑOS</b>
									<ul>
										<li>USS Tunal</li>
										<li>USS Fontibón</li>
										<li>USS. Kennedy</li>
										<li>USS San Blas</li>
										<li>USS. La Victoria</li>
										<li>USS. Simón Bolívar</li>
										<li>USS. Chapinero</li>
									</ul>
								</p>
                            </div>
                        </div> 
                    </div>--> 
                    <?php
                    $preguntas_1 = $this->covid_model->preguntas_ira(1);                
                    ?>
                    <div class="row" id="div_ira1">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    Fiebre cuantificada mayor o igual a 38°C
                                </div>
                                <div class="card-body">
                                <?php
                                    for($i=0;$i<count($preguntas_1);$i++){										
                                    ?>             
                                    <div class="row border-bottom <?php if($i>0){ echo "d-none";}?>" id="div_preg_ira_<?php echo $preguntas_1[$i]->id_sintoma?>">
                                        <div class="col-md-6">
                                            <label for="nombre"><?php echo $preguntas_1[$i]->pregunta?></label><br>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input validate[required]" type="radio" name="sintoma<?php echo $preguntas_1[$i]->id_sintoma?>" id="sintoma<?php echo $preguntas_1[$i]->id_sintoma?>" value="1" onclick="validarSintoma(this.id, <?php echo $preguntas_1[$i]->id_sintoma?>, this.value)">
                                                <label class="form-check-label" for="inlineRadio1">Si</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input validate[required]" type="radio" name="sintoma<?php echo $preguntas_1[$i]->id_sintoma?>" id="sintoma<?php echo $preguntas_1[$i]->id_sintoma?>" value="0" onclick="validarSintoma(this.id, <?php echo $preguntas_1[$i]->id_sintoma?>, this.value)">
                                                <label class="form-check-label" for="inlineRadio2">No</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6" id="div_resultado_<?php echo $preguntas_1[$i]->id_sintoma?>">
                                        </div>
										<hr>
                                    </div>
                                    <?php
                                    }
                                ?>
                                </div>
                            </div>									
                        </div>
                    </div>
                    <?php
                    $preguntas_2 = $this->covid_model->preguntas_ira(2);                
                    ?>
                    <div class="row" id="div_ira2">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    Tos
                                </div>
                                <div class="card-body">
                                <?php
                                    for($i=0;$i<count($preguntas_2);$i++){
                                    ?>                                        
                                        <div class="row border-bottom <?php if($i>0){ echo "d-none";}?>" id="div_preg_ira_<?php echo $preguntas_2[$i]->id_sintoma?>">
                                            <div class="col-md-6">
                                                <label for="nombre"><?php echo $preguntas_2[$i]->pregunta?></label><br>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input validate[required]" type="radio" name="sintoma<?php echo $preguntas_2[$i]->id_sintoma?>" id="sintoma<?php echo $preguntas_2[$i]->id_sintoma?>" value="1" onclick="validarSintoma(this.id, <?php echo $preguntas_2[$i]->id_sintoma?>, this.value)">
                                                    <label class="form-check-label" for="inlineRadio1">Si</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input validate[required]" type="radio" name="sintoma<?php echo $preguntas_2[$i]->id_sintoma?>" id="sintoma<?php echo $preguntas_2[$i]->id_sintoma?>" value="0" onclick="validarSintoma(this.id, <?php echo $preguntas_2[$i]->id_sintoma?>, this.value)">
                                                    <label class="form-check-label" for="inlineRadio2">No</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="div_resultado_<?php echo $preguntas_2[$i]->id_sintoma?>">
                                            </div>
											<hr>
                                        </div>
                                    <?php
                                    }
                                ?>
                                </div>
                            </div>									
                        </div>
                    </div>
                    <?php
                    $preguntas_3 = $this->covid_model->preguntas_ira(3);                
                    ?>
                    <div class="row" id="div_ira3">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    Dificultad para respirar
                                </div>
                                <div class="card-body">
                                <?php
                                    for($i=0;$i<count($preguntas_3);$i++){
                                    ?>                                        
                                        <div class="row border-bottom <?php if($i>0){ echo "d-none";}?>" id="div_preg_ira_<?php echo $preguntas_3[$i]->id_sintoma?>">
                                            <div class="col-md-6">
                                                <label for="nombre"><?php echo $preguntas_3[$i]->pregunta?></label><br>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input validate[required]" type="radio" name="sintoma<?php echo $preguntas_3[$i]->id_sintoma?>" id="sintoma<?php echo $preguntas_3[$i]->id_sintoma?>" value="1" onclick="validarSintoma(this.id, <?php echo $preguntas_3[$i]->id_sintoma?>, this.value)">
                                                    <label class="form-check-label" for="inlineRadio1">Si</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input validate[required]" type="radio" name="sintoma<?php echo $preguntas_3[$i]->id_sintoma?>" id="sintoma<?php echo $preguntas_3[$i]->id_sintoma?>" value="0" onclick="validarSintoma(this.id, <?php echo $preguntas_3[$i]->id_sintoma?>, this.value)">
                                                    <label class="form-check-label" for="inlineRadio2">No</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="div_resultado_<?php echo $preguntas_3[$i]->id_sintoma?>">
                                            </div>
											<hr>
                                        </div>
                                    <?php
                                    }
                                ?>
                                </div>
                            </div>									
                        </div>
                    </div>
                    <?php
                    $preguntas_4 = $this->covid_model->preguntas_ira(4);                
                    ?>
                    <div class="row" id="div_ira4">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                   Antecedentes
                                </div>
                                <div class="card-body">
                                <?php
                                    for($i=0;$i<count($preguntas_4);$i++){
                                    ?>                                        
                                        <div class="row border-bottom <?php if($i>0){ echo "d-none";}?>" id="div_preg_ira_<?php echo $preguntas_4[$i]->id_sintoma?>">
                                            <div class="col-md-6">
                                                <label for="nombre"><?php echo $preguntas_4[$i]->pregunta?></label><br>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input validate[required]" type="radio" name="sintoma<?php echo $preguntas_4[$i]->id_sintoma?>" id="sintoma<?php echo $preguntas_4[$i]->id_sintoma?>" value="1" onclick="validarSintoma(this.id, <?php echo $preguntas_4[$i]->id_sintoma?>, this.value)">
                                                    <label class="form-check-label" for="inlineRadio1">Si</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input validate[required]" type="radio" name="sintoma<?php echo $preguntas_4[$i]->id_sintoma?>" id="sintoma<?php echo $preguntas_4[$i]->id_sintoma?>" value="0" onclick="validarSintoma(this.id, <?php echo $preguntas_4[$i]->id_sintoma?>, this.value)">
                                                    <label class="form-check-label" for="inlineRadio2">No</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="div_resultado_<?php echo $preguntas_4[$i]->id_sintoma?>">
                                            </div>
											<hr>
										</div>                                        
                                    <?php
                                    }
                                ?>
                                </div>
                            </div>									
                        </div>
                    </div>
					<?php
                    $preguntas_5 = $this->covid_model->preguntas_ira(5);                
                    ?>
                    <div class="row" id="div_ira5">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    Estado de compotamiento y actitud
                                </div>
                                <div class="card-body">
                                <?php
                                    for($i=0;$i<count($preguntas_5);$i++){										
                                    ?>                                        
                                        <div class="row border-bottom <?php if($i>0){ echo "d-none";}?>" id="div_preg_ira_<?php echo $preguntas_5[$i]->id_sintoma?>">
                                            <div class="col-md-6">
                                                <label for="nombre"><?php echo $preguntas_5[$i]->pregunta?></label><br>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input validate[required]" type="radio" name="sintoma<?php echo $preguntas_5[$i]->id_sintoma?>" id="sintoma<?php echo $preguntas_5[$i]->id_sintoma?>" value="1" onclick="validarSintoma(this.id, <?php echo $preguntas_5[$i]->id_sintoma?>, this.value)">
                                                    <label class="form-check-label" for="inlineRadio1">Si</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input validate[required]" type="radio" name="sintoma<?php echo $preguntas_5[$i]->id_sintoma?>" id="sintoma<?php echo $preguntas_5[$i]->id_sintoma?>" value="0" onclick="validarSintoma(this.id, <?php echo $preguntas_5[$i]->id_sintoma?>, this.value)">
                                                    <label class="form-check-label" for="inlineRadio2">No</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="div_resultado_<?php echo $preguntas_5[$i]->id_sintoma?>">
                                            </div>
											<hr>
                                        </div>
                                    <?php
                                    }
                                ?>
                                </div>
                            </div>									
                        </div>
                    </div>
					<?php
                    $preguntas_6 = $this->covid_model->preguntas_ira(6);                
                    ?>
                    <div class="row" id="div_ira6">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    Alimentación
                                </div>
                                <div class="card-body">
                                <?php
                                    for($i=0;$i<count($preguntas_6);$i++){
                                    ?>                                        
                                        <div class="row border-bottom <?php if($i>0){ echo "d-none";}?>" id="div_preg_ira_<?php echo $preguntas_6[$i]->id_sintoma?>">
                                            <div class="col-md-6">
                                                <label for="nombre"><?php echo $preguntas_6[$i]->pregunta?></label><br>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input validate[required]" type="radio" name="sintoma<?php echo $preguntas_6[$i]->id_sintoma?>" id="sintoma<?php echo $preguntas_6[$i]->id_sintoma?>" value="1" onclick="validarSintoma(this.id, <?php echo $preguntas_6[$i]->id_sintoma?>, this.value)">
                                                    <label class="form-check-label" for="inlineRadio1">Si</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input validate[required]" type="radio" name="sintoma<?php echo $preguntas_6[$i]->id_sintoma?>" id="sintoma<?php echo $preguntas_6[$i]->id_sintoma?>" value="0" onclick="validarSintoma(this.id, <?php echo $preguntas_6[$i]->id_sintoma?>, this.value)">
                                                    <label class="form-check-label" for="inlineRadio2">No</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="div_resultado_<?php echo $preguntas_6[$i]->id_sintoma?>">
                                            </div>
											<hr>
                                        </div>
                                        
                                    <?php
                                    }
                                ?>
                                </div>
                            </div>									
                        </div>
                    </div>
					<div class="row" id="div_conf">
                        <div class="col">
                            <label for="nombre">Confirmar correo electrónico</label>
                            <input type="text" id="correo_conf" name="correo_conf" class="form-control validate[required, custom[email], equals[correo]]">
                        </div>
					</div>
                    <div class="row" id="div_enviar">
                        <hr>
                        <div class="text-center">
                            <button class="btn btn-success">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col">
        <div class="card">
            <div class="card-header">
                Conoce los puntos de atención
            </div>
            <div class="card-body">
                 <iframe width="100%" height="800px" src=" https://sdsgissaludbog.maps.arcgis.com/apps/View/index.html?appid=c51e7476bad54b83bd8f5fa8f0533355" frameborder="0" scrolling="no"></iframe>
            </div>
        </div>
    </div>
</div>

