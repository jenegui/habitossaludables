<div class="row">    
    <div class="col">
        <div class="card">
            <div class="card-header">
                
            </div>
            <div class="card-body">
                <form class="form" name="formContactoCol" id="formContactoCol" method="post" action="<?php echo base_url('habitos/procesarFormCol')?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col">
                            <label for="nombre">Número de identificación</label>
                            <input type="text" id="identificacion" name="identificacion" class="form-control validate[required, minSize[3], custom[onlyNumberSp]]">
                        </div>    
                        <div class="col">
                            <label for="nombre">Nombre</label>
                            <input type="text" id="nombre" name="nombre" class="form-control validate[required, minSize[5], custom[onlyLetterSp]]">
                        </div>
                    </div>
                    <div class="row">                            
                        <div class="col">
                            <label for="nombre">Correo Electrónico</label>
                            <input type="text" id="correo" name="correo" class="form-control validate[required, custom[email]]">
                        </div>
                    </div>
                    <div class="row">    
                        <div class="col">
                            <label for="nombre">Teléfono</label>
                            <input type="text" id="telefono" name="telefono" class="form-control validate[required, maxSize[10], minSize[7], custom[onlyNumberSp]]">
                        </div>
                        <div class="col">
                            <label for="nombre">Celular</label>
                            <input type="text" id="celular" name="celular" class="form-control validate[required, maxSize[16], minSize[7], custom[onlyNumberSp]]">
                        </div>  
                    </div>
                    <div class="row">                           
                        <div class="col">
                            <label for="nombre">Dirección</label>
                            <input type="text" id="direccion" name="direccion" class="form-control validate[required, minSize[5]]">
                        </div>
                    </div>
                    <div class="row" id="div_preg1">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Como empresas y espacios laborales organizan trabajo en casa de todos los empleados que le sea posible? </label><br>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta1" id="pregunta1" value="1">
                              <label class="form-check-label" for="inlineRadio1">Si</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta1" id="pregunta1" value="0">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>							
                        </div>                   
                    </div>
                    <div class="row" id="div_preg2">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Para los empleados que sea indispensable que asistan al lugar de trabajo, se organizan al menos 3 turnos de entrada y salida a lo largo del día laboral? </label><br>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta2" id="pregunta2" value="1">
                              <label class="form-check-label" for="inlineRadio1">Si</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta2" id="pregunta2" value="0">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>							
                        </div>                   
                    </div>
                    <div class="row" id="div_preg3">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Además del trabajo en casa y turnos de ingreso y salida, las universidades y colegios organizan la virtualización de tantas clases y actividades como les sea posible? </label><br>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta3" id="pregunta3" value="1">
                              <label class="form-check-label" for="inlineRadio1">Si</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta3" id="pregunta3" value="0">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>							
                        </div>                   
                    </div>
                    <div class="row" id="div_preg4">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Aplaza todo tipo de evento público o privado de concentración masiva, de más de mil personal, en contacto estrecho, es decir a menos de 2 metros por más de 15 minutos? </label><br>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta4" id="pregunta4" value="1">
                              <label class="form-check-label" for="inlineRadio1">Si</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta4" id="pregunta4" value="0">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>							
                        </div>                   
                    </div>
                    <div class="row" id="div_enviar">
                        <hr>
                        <div class="text-center">
                            <button class="btn btn-success">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>