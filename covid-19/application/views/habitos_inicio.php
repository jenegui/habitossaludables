<div class="row">
	<div class="alert alert-info" role="alert">
		<p>
			Recuerde que la información aportada puede estar sometida a cambios dinámicos basados en los procesos de investigación reportado por la OPS y OMS y los lineamientos establecidos por el Ministerio de Salud y el Instituto Nacional de Salud. Para más información diríjase a las siguientes fuentes oficiales
		</p>
		<ul>
			<li>Ministerio Nacional de Salud:  
				<ul>
					<li><a href="https://www.minsalud.gov.co/sites/rid/Lists/BibliotecaDigital/RIDE/VS/preguntas-rtas-coronavirus.pdf" target="_blank">Preguntas frecuentes</li></li>
					<li><a href="https://www.minsalud.gov.co/sites/rid/Lists/BibliotecaDigital/RIDE/VS/PP/ET/abece-coronavirus.pdf" target="_blank">Abecé</li></li>
					<li><a href="https://www.minsalud.gov.co/salud/publica/PET/Paginas/Nuevo-Coronavirus-nCoV.aspx" target="_blank">Campañas</li></li>
					<li><a href="https://www.minsalud.gov.co/salud/publica/PET/Paginas/Nuevo-Coronavirus-nCoV.aspx" target="_blank">Videos</li></li> 
				</ul> 
			<li>Instituto Nacional de Salud: <a href="https://www.ins.gov.co/Noticias/Coronavirus/abece-coronavirus.pdf" target="_blank">Abecé - Coronavirus</a></li>
			<li><a href="http://www.saludcapital.gov.co/Paginas2/Coronavirus.aspx" target="_blank">Secretaria Distrital de Salud</a></li>
		</ul>
	</div>
</div>
<div class="row">
	<div class="col">
		<div id="carouselExampleControls" class="carousel carousel-with-lb slide " data-ride="carousel">
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img src="<?php echo base_url('assets/imgs/habitos/img1.jpg')?>" class="d-block w-100">
				</div>
				<div class="carousel-item">
					<img src="<?php echo base_url('assets/imgs/habitos/img2.jpg')?>" class="d-block w-100">
				</div>
				<div class="carousel-item">
					<img src="<?php echo base_url('assets/imgs/habitos/img3.jpg')?>" class="d-block w-100" alt="...">
				</div>
			</div>
			<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previa</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Siguiente</span>
			</a>
		</div>
	</div>	
</div>
<?php

$retornoError = $this->session->flashdata('error');
if ($retornoError) {
?>
    <div class="alert alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <?php echo $retornoError ?>
    </div>
    <?php
}

$retornoExito = $this->session->flashdata('exito');
if ($retornoExito) {
?>
    <div class="alert alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
        <?php echo $retornoExito ?>
    </div>
    <?php
}
    
?>
<div class="row">    
    <div class="col">
        <div class="card">
            <div class="card-header">
                REPORTE DE AUTOCUIDADO
            </div>
            <div class="card-body">
            	<div class="container">
					<div id="tabs" class="skltbs">
					  <ul class="skltbs-tab-group">
					    <li><a href="#tabs-1">Autocuidado individual</a></li></li>
					    <li><a href="#tabs-2">Autocuidado colectivo&nbsp;</a></li>
					    <li><a href="#tabs-3">Seguimiento reporte&nbsp;&nbsp;&nbsp;&nbsp;</a></li><!--Seguimiento reporte&nbsp;&nbsp;&nbsp;-->
					  </ul>
					  <div id="tabs-1">
					    	<?php $this->load->view("individual"); ?>
					  	</div>
					  	<div id="tabs-2">
					    	<?php $this->load->view("colectivo"); ?>
					  	</div>
					  	<div id="tabs-3">
					    	<?php $this->load->view("form_seguimiento"); ?>
					  	</div>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>        	
<div class="row">
	<div class="col">
        <div class="card">
            <div class="card-header">
                Conoce los puntos de atención
            </div>
            <div class="card-body">
                 <iframe width="100%" height="800px" src=" https://sdsgissaludbog.maps.arcgis.com/apps/View/index.html?appid=c51e7476bad54b83bd8f5fa8f0533355" frameborder="0" scrolling="no"></iframe>
            </div>
        </div>
    </div>
</div>

