<div class="row">    
    <div class="col">
        <div class="card">
            <div class="card-header">
                
            </div>
            <div class="card-body">
                <form class="form" name="formContactoInd" id="formContactoInd" method="post" action="<?php echo base_url('habitos/procesarFormInd')?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col">
                            <label for="nombre">Número de identificación</label>
                            <input type="text" id="identificacion" name="identificacion" class="form-control validate[required, minSize[3], custom[onlyNumberSp]]">
                        </div>    
                        <div class="col">
                            <label for="nombre">Nombre</label>
                            <input type="text" id="nombre" name="nombre" class="form-control validate[required, minSize[5], custom[onlyLetterSp]]">
                        </div>
                    </div>
                    <div class="row">                            
                        <div class="col">
                            <label for="nombre">Correo Electrónico</label>
                            <input type="text" id="correo" name="correo" class="form-control validate[required, custom[email]]">
                        </div>
                    </div>
                    <div class="row">      
                        <div class="col">
                            <label for="nombre">Teléfono</label>
                            <input type="text" id="telefono" name="telefono" class="form-control validate[required, maxSize[10], minSize[7], custom[onlyNumberSp]]">
                        </div>
                        <div class="col">
                            <label for="nombre">Celular</label>
                            <input type="text" id="celular" name="celular" class="form-control validate[required, maxSize[16], minSize[7], custom[onlyNumberSp]]">
                        </div>  
                    </div>
                    <div class="row">                           
                        <div class="col">
                            <label for="nombre">Dirección</label>
                            <input type="text" id="direccion" name="direccion" class="form-control validate[required, minSize[5]]">
                        </div>
                        <div class="col">
                            <label for="nombre">EPS</label>
                            <input type="text" id="eps" name="eps" class="form-control validate[required, minSize[5], custom[onlyLetterSp]]">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="enc_fech_nac">Fecha de Nacimiento</label>
                            <input type="date" class="form-control validate[required]" id="enc_fech_nac" name="enc_fech_nac" placeholder="AAAA-MM-DD"> 
                            <input type="hidden" id="edad" name="edad">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                        	<hr>
                            Cada persona debe realizar una pausa activa con las siguientes acciones:
                        </div>
                    </div>
                    <div class="row" id="div_preg1">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Cada tres horas, se lava las manos con abundante jab&oacute;n, alcohol o gel antis&eacute;ptico? </label><br>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta1" id="pregunta1" value="1">
                              <label class="form-check-label" for="inlineRadio1">Si</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta1" id="pregunta1" value="0">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>							
                        </div>                   
                    </div>
                    <div class="row" id="div_preg2">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Toma agua (se hidrata)? </label><br>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta2" id="pregunta2" value="1">
                              <label class="form-check-label" for="inlineRadio1">Si</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta2" id="pregunta2" value="0">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>							
                        </div>                   
                    </div>
                    <div class="row" id="div_preg3">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Se tapa la nariz y boca con el antebrazo (no con la mano) al estornudar o toser? </label><br>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta3" id="pregunta3" value="1">
                              <label class="form-check-label" for="inlineRadio1">Si</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta3" id="pregunta3" value="0">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>							
                        </div>                   
                    </div>
                    <div class="row" id="div_preg4">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Evita contacto directo, no saluda de beso o de mano, no da abrazos? </label><br>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta4" id="pregunta4" value="1">
                              <label class="form-check-label" for="inlineRadio1">Si</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta4" id="pregunta4" value="0">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>							
                        </div>                   
                    </div>
                    <div class="row" id="div_preg6">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Evita asistir a eventos masivos o de cualquier tipo que no sean indispensables? </label><br>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta5" id="pregunta5" value="1">
                              <label class="form-check-label" for="inlineRadio1">Si</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta5" id="pregunta5" value="0">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>							
                        </div>                   
                    </div>
                     <div class="row" id="div_preg6">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿En caso de gripa, usa tapabocas y se queda en casa? </label><br>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta6" id="pregunta6" value="1">
                              <label class="form-check-label" for="inlineRadio1">Si</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input validate[required]" type="radio" name="pregunta6" id="pregunta6" value="0">
                              <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>							
                        </div>                   
                    </div>
                    <div class="row" id="div_enviar">
                        <hr>
                        <div class="text-center">
                            <button class="btn btn-success">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>