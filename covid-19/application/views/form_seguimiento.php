<div class="row">    
    <div class="col">
        <div class="card">
            <div class="card-header">
                
            </div>
            <div class="card-body">
                <form class="form" name="formSeguimiento" id="formSeguimiento" method="post" action="<?php echo base_url('habitos/procesarSeguimiento')?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col">
                            <label for="nombre">Número de identificación</label>
                            <input type="text" id="identificacion" name="identificacion" class="form-control validate[required, minSize[3], custom[onlyNumberSp]]">
                        </div>    
                        <!--<div class="col">
                            <label for="nombre">Celular</label>
                            <input type="text" id="celular" name="celular" class="form-control validate[required, maxSize[16], minSize[7], custom[onlyNumberSp]]">
                        </div> --> 
                    </div>
                    <br>
                    <div class="row" id="div_enviar">
                        <hr>
                        <div class="text-center">
                            <button class="btn btn-success">Consultar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>