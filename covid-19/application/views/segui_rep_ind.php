<div class="row">    
    <div class="col">
        <div class="card">
            <div class="card-header">
                
            </div>
            <div class="card-body">
               
                	 <div class="row">
                        <div class="col">
                        	
                            Seguimiento al reporte de autocuidado:
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="nombre">Nombre:</label>
                            <label class="form-check-label" for="inlineRadio1"><?php echo $nombre; ?></label>
                        </div>    
                        <div class="col">
                            <label for="nombre">Identificaci&oacute;:</label>
                            <label class="form-check-label" for="inlineRadio1"><?php echo $identificacion; ?></label>
                        </div>
                    </div>
                   	<div class="row" id="div_preg1">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Cada tres horas, se lava las manos con abundante jab&oacute;n, alcohol o gel antis&eacute;ptico? </label><br>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label" for="inlineRadio1"><?php echo $pregunta1==1?'SI':'NO' ?></label>
                            </div>
                        </div>                   
                    </div>
                    <div class="row" id="div_preg2">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Toma agua (se hidrata)? </label><br>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label" for="inlineRadio1"><?php echo $pregunta2==1?'SI':'NO' ?></label>
                            </div>
                        </div>                   
                    </div>
                    <div class="row" id="div_preg3">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Se tapa la nariz y boca con el antebrazo (no con la mano) al estornudar o toser? </label><br>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label" for="inlineRadio1"><?php echo $pregunta3==1?'SI':'NO' ?></label>
                            </div>
                        </div>                   
                    </div>
                    <div class="row" id="div_preg4">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Evita contacto directo, no saluda de beso o de mano, no da abrazos? </label><br>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label" for="inlineRadio1"><?php echo $pregunta4==1?'SI':'NO' ?></label>
                            </div>
                        </div>                   
                    </div>
                    <div class="row" id="div_preg6">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿Evita asistir a eventos masivos o de cualquier tipo que no sean indispensables? </label><br>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label" for="inlineRadio1"><?php echo $pregunta5==1?'SI':'NO' ?></label>
                            </div>
                        </div>                   
                    </div>
                     <div class="row" id="div_preg6">                        
                        <div class="col-md-12">
							<hr>
                            <label>¿En caso de gripa, usa tapabocas y se queda en casa? </label><br>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label" for="inlineRadio1"><?php echo $pregunta6==1?'SI':'NO' ?></label>
                            </div>
                        </div>                   
                    </div>
                    <div class="row" id="div_enviar">
                        <hr>
                        <div class="text-center">
                            <a class="btn btn-success" href=" <?php echo base_url(). 'habitos/'; ?> "><span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Regresar </a> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>