<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Habitos extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    /**
     * [__construct description]
     *
     * @method __construct
     */
    public function __construct()
    {
        // Load the constructer from MY_Controller
        parent::__construct();
        $this->load->database('default');
        $this->load->model('covid_model');
        $this->load->helper('url');
    }

    /**
     * [index description]
     *
     * @method index
     *
     * @return [type] [description]
     */
	public function index()
	{
        //
        $data['js'] = array(base_url('assets/js/covid.js'));
        $data['titulo'] = 'Covid-19';
        $data['contenido'] = "habitos_inicio";
        $data['controller']="habitos";
		$this->load->view('layout_principal', $data);
	}
        
    public function procesarFormInd(){
        $data['identificacion'] = $_REQUEST['identificacion'];
        $data['nombre'] = $_REQUEST['nombre'];        
        $data['correo'] = $_REQUEST['correo'];
        
        $data['telefono'] = $_REQUEST['telefono'];
        $data['celular'] = $_REQUEST['celular'];
        $data['direccion'] = $_REQUEST['direccion'];
        $data['eps'] = $_REQUEST['eps'];
        $data['fecha_nacimiento'] = $_REQUEST['enc_fech_nac'];
        $data['edad'] = $_REQUEST['edad'];
        
        if(isset($_REQUEST['pregunta1'])){
            $data['pregunta1'] = $_REQUEST['pregunta1'];
            
        }else{
			$data['pregunta1'] = NULL;
		}
        
        if(isset($_REQUEST['pregunta3'])){
            $data['pregunta2'] = $_REQUEST['pregunta2'];
        }else{
			$data['pregunta2'] = NULL;
		}

        if(isset($_REQUEST['pregunta3'])){
            $data['pregunta3'] = $_REQUEST['pregunta3'];
        }else{
            $data['pregunta3'] = NULL;
        }

        if(isset($_REQUEST['pregunta4'])){
            $data['pregunta4'] = $_REQUEST['pregunta4'];
        }else{
            $data['pregunta4'] = NULL;
        }

        if(isset($_REQUEST['pregunta5'])){
            $data['pregunta5'] = $_REQUEST['pregunta5'];
        }else{
            $data['pregunta5'] = NULL;
        }

        if(isset($_REQUEST['pregunta6'])){
            $data['pregunta6'] = $_REQUEST['pregunta6'];
        }else{
            $data['pregunta6'] = NULL;
        }

        
		$data['fecha_registro'] = date('Y-m-d H:i:s');
        
        $resultado = $this->covid_model->registrarIndividual($data);
        
        if($resultado){
			
			$datos_tramite['nombre'] = $_REQUEST['nombre'];
			$datos_tramite['email'] = $_REQUEST['correo'];
			$this->enviarCorreo($datos_tramite);

            if($_REQUEST['pregunta1'] == 0 || $_REQUEST['pregunta2'] == 0 || $_REQUEST['pregunta3'] == 0
            || $_REQUEST['pregunta4'] == 0 || $_REQUEST['pregunta5'] == 0 || $_REQUEST['pregunta6'] == 0){
    			$datos_alerta['identificacion'] = $_REQUEST['identificacion'];
                $datos_alerta['nombre'] = $_REQUEST['nombre'];        
                $datos_alerta['correo'] = $_REQUEST['correo'];
                $datos_alerta['telefono'] = $_REQUEST['telefono'];
                $datos_alerta['celular'] = $_REQUEST['celular'];
                $datos_alerta['direccion'] = $_REQUEST['direccion'];
                $datos_alerta['eps'] = $_REQUEST['eps'];
                $datos_alerta['fecha_nacimiento'] = $_REQUEST['enc_fech_nac'];
                $datos_alerta['edad'] = $_REQUEST['edad'];
                $celular=$_REQUEST['celular'];
                if($_REQUEST['pregunta1']==0){
                    $mensaje="RECUERDE QUE: Cada 3 horas, debe lavarse las manos con abundante jabón, alcohol o gel antiséptico.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                    $datos_alerta['pregunta1'] = "Cada 3 horas, debe lavarse las manos con abundante jabón, alcohol o gel antiséptico.";
                }
                if($_REQUEST['pregunta2']==0){
                    $mensaje="RECUERDE QUE: Debe tomar agua (hidratarse).";
                    $this->envia_sms_mensaje($celular, $mensaje);
                    $datos_alerta['pregunta2'] = "Tomar agua (hidratarse).";
                }
                if($_REQUEST['pregunta3']==0){
                    $mensaje="RECUERDE QUE: Debe taparse la nariz y boca con el antebrazo (no con la mano) al estornudar o toser.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                    $datos_alerta['pregunta3'] = "Taparse la nariz y boca con el antebrazo (no con la mano) al estornudar o toser.";
                }
                if($_REQUEST['pregunta4']==0){
                    $mensaje="RECUERDE QUE: Es importante evitar contacto directo, no saludar de beso o de mano, no dar abrazos.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                    $datos_alerta['pregunta4'] = "Evitar contacto directo, no saludar de beso o de mano, no dar abrazos.";
                }
                if($_REQUEST['pregunta5']==0){
                    $mensaje="RECUERDE QUE: Es importante evitar asistir a eventos masivos o de cualquier tipo que no sean indispensables.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                    $datos_alerta['pregunta5'] = "Evitar asistir a eventos masivos o de cualquier tipo que no sean indispensables.";
                }
                if($_REQUEST['pregunta6']==0){
                    $mensaje="RECUERDE QUE: En caso de gripa, debe usar tapabocas y quedarse en casa.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                    $datos_alerta['pregunta6'] = "En caso de gripa, debe usar tapabocas y quedarse en casa.";
                }
                $this->enviarCorreoAlerta($datos_alerta);
            }    

            $this->session->set_flashdata('exito', 'El registro se realizó exitosamente...');
            redirect(base_url('habitos/#tabs-1'), 'refresh');
            exit;    
        }else{
            $this->session->set_flashdata('error', 'No fue posible realizar el registro de información');
            redirect(base_url('habitos/#tabs-1'), 'refresh');
            exit; 
        }
        
        
    }


    public function procesarFormCol(){
        $data['identificacion'] = $_REQUEST['identificacion'];
        $data['nombre'] = $_REQUEST['nombre'];        
        $data['correo'] = $_REQUEST['correo'];
        
        $data['telefono'] = $_REQUEST['telefono'];
        $data['celular'] = $_REQUEST['celular'];
        $data['direccion'] = $_REQUEST['direccion'];
        
        if(isset($_REQUEST['pregunta1'])){
            $data['pregunta1'] = $_REQUEST['pregunta1'];
            
        }else{
            $data['pregunta1'] = NULL;
        }
        
        if(isset($_REQUEST['pregunta2'])){
            $data['pregunta2'] = $_REQUEST['pregunta2'];
        }else{
            $data['pregunta2'] = NULL;
        }

        if(isset($_REQUEST['pregunta3'])){
            $data['pregunta3'] = $_REQUEST['pregunta3'];
        }else{
            $data['pregunta3'] = NULL;
        }

        if(isset($_REQUEST['pregunta4'])){
            $data['pregunta4'] = $_REQUEST['pregunta4'];
        }else{
            $data['pregunta4'] = NULL;
        }

                
        
        $data['fecha_registro'] = date('Y-m-d H:i:s');
        
        $resultado = $this->covid_model->registrarColectivo($data);
        
        if($resultado){
            
            $datos_tramite['nombre'] = $_REQUEST['nombre'];
            $datos_tramite['email'] = $_REQUEST['correo'];
            $this->enviarCorreo($datos_tramite);
            
            if($_REQUEST['pregunta1'] == 0 || $_REQUEST['pregunta2'] == 0 || $_REQUEST['pregunta3'] == 0 || $_REQUEST['pregunta4'] == 0 ){
                $datos_alerta['identificacion'] = $_REQUEST['identificacion'];
                $datos_alerta['nombre'] = $_REQUEST['nombre'];        
                $datos_alerta['correo'] = $_REQUEST['correo'];
                $datos_alerta['telefono'] = $_REQUEST['telefono'];
                $datos_alerta['celular'] = $_REQUEST['celular'];
                $datos_alerta['direccion'] = $_REQUEST['direccion'];
                $celular=$_REQUEST['celular'];
                if($_REQUEST['pregunta1']==0){
                    $mensaje="RECUERDE QUE: Como empresas y espacios laborales deben organizar trabajo en casa de todos los empleados que le sea posible.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                    $datos_alerta['pregunta1'] = "Como empresas y espacios laborales deben organizar trabajo en casa de todos los empleados que le sea posible.";
                }
                if($_REQUEST['pregunta2']==0){
                    $mensaje="RECUERDE QUE: Para los empleados que sea indispensable que asistan al lugar de trabajo, se debe organizar al menos 3 turnos de entrada y salida a lo largo del día laboral.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                    $datos_alerta['pregunta2'] = "Para los empleados que sea indispensable que asistan al lugar de trabajo, se debe organizar al menos 3 turnos de entrada y salida a lo largo del día laboral.";
                }
                if($_REQUEST['pregunta3']==0){
                    $mensaje="RECUERDE QUE: Además del trabajo en casa y turnos de ingreso y salida, las universidades y colegios deben organizar la virtualización de tantas clases y actividades como les sea posible.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                    $datos_alerta['pregunta3'] = "Además del trabajo en casa y turnos de ingreso y salida, las universidades y colegios deben organizar la virtualización de tantas clases y actividades como les sea posible.";
                }
                if($_REQUEST['pregunta4']==0){
                    $mensaje="RECUERDE QUE: Es importante aplazar todo tipo de evento público o privado de concentración masiva, de más de mil personal, en contacto estrecho, es decir a menos de 2 metros por más de 15 minutos.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                    $datos_alerta['pregunta4'] = "Aplazar todo tipo de evento público o privado de concentración masiva, de más de mil personal, en contacto estrecho, es decir a menos de 2 metros por más de 15 minutos.";
                }
                $this->enviarCorreoAlerta($datos_alerta);
            }    
            
            $this->session->set_flashdata('exito', 'El registro ser realizó exitosamente...');
            redirect(base_url('habitos/#tabs-2'), 'refresh');
            exit;    
        }else{
            $this->session->set_flashdata('error', 'No fue posible realizar el registro de información');
            redirect(base_url('habitos/#tabs-2'), 'refresh');
            exit; 
        }
        
        
    }

    public function procesarSeguimiento(){
        $data['js'] = array(base_url('assets/js/covid.js'));
        $data['titulo'] = 'Covid-19';
        $data['controller']="habitos";
        $identificacion = $_REQUEST['identificacion'];
        
        $listado_ind = $this->covid_model->registroIndividual($identificacion);
        $listado_col = $this->covid_model->registrocolectivo($identificacion);
        if(count($listado_ind) > 0){
            $data['contenido'] = "segui_rep_ind";
            foreach($listado_ind as $l){
                $data['identificacion']=$l->identificacion;
                $data['nombre']=$l->nombre;
                $data['correo']=$l->correo;
                $data['celular']=$l->celular;
                $data['pregunta1']=$l->pregunta1;
                $data['pregunta2']=$l->pregunta2;
                $data['pregunta3']=$l->pregunta3;
                $data['pregunta4']=$l->pregunta4;
                $data['pregunta5']=$l->pregunta5;
                $data['pregunta6']=$l->pregunta6;
                $celular=$data['celular'];
                if($data['pregunta1']==0){
                    $mensaje="RECUERDE QUE: Cada 3 horas, debe lavarse las manos con abundante jabón, alcohol o gel antiséptico.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                }
                if($data['pregunta2']==0){
                    $mensaje="RECUERDE QUE: Debe tomar agua (hidratarse).";
                    $this->envia_sms_mensaje($celular, $mensaje);
                }
                if($data['pregunta3']==0){
                    $mensaje="RECUERDE QUE: Debe taparse la nariz y boca con el antebrazo (no con la mano) al estornudar o toser.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                }
                if($data['pregunta4']==0){
                    $mensaje="RECUERDE QUE: Es importante evitar contacto directo, no saludar de beso o de mano, no dar abrazos.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                }
                if($data['pregunta5']==0){
                    $mensaje="RECUERDE QUE: Es importante evitar asistir a eventos masivos o de cualquier tipo que no sean indispensables.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                }
                if($data['pregunta6']==0){
                    $mensaje="RECUERDE QUE: En caso de gripa, debe usar tapabocas y quedarse en casa.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                }
            }   

        }

        if(count($listado_col) > 0){
            $data['contenido'] = "segui_rep_col";
            foreach($listado_col as $l){
                $data['identificacion']=$l->identificacion;
                $data['nombre']=$l->nombre;
                $data['correo']=$l->correo;
                $data['celular']=$l->celular;
                $data['pregunta1']=$l->pregunta1;
                $data['pregunta2']=$l->pregunta2;
                $data['pregunta3']=$l->pregunta3;
                $data['pregunta4']=$l->pregunta4;
                $celular=$data['celular'];
                if($data['pregunta1']==0){
                    $mensaje="RECUERDE QUE: Como empresas y espacios laborales deben organizar trabajo en casa de todos los empleados que le sea posible.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                }
                if($data['pregunta2']==0){
                    $mensaje="RECUERDE QUE: Para los empleados que sea indispensable que asistan al lugar de trabajo, se debe organizar al menos 3 turnos de entrada y salida a lo largo del día laboral.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                }
                if($data['pregunta3']==0){
                    $mensaje="RECUERDE QUE: Además del trabajo en casa y turnos de ingreso y salida, las universidades y colegios deben organizar la virtualización de tantas clases y actividades como les sea posible.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                }
                if($data['pregunta4']==0){
                    $mensaje="RECUERDE QUE: Es importante aplazar todo tipo de evento público o privado de concentración masiva, de más de mil personal, en contacto estrecho, es decir a menos de 2 metros por más de 15 minutos.";
                    $this->envia_sms_mensaje($celular, $mensaje);
                }
            }   

        }

        $this->load->view('layout_principal', $data);
    }    

    public function envia_sms_mensaje($celular, $mensaje){
        $curl = curl_init();

        curl_setopt_array($curl, array(
         CURLOPT_URL => "https://appb.saludcapital.gov.co/WebApiRestMensajeriaSMS/api/mensajeriaSms/enviarMsj",
         CURLOPT_RETURNTRANSFER => true,
         CURLOPT_ENCODING => "",
         CURLOPT_MAXREDIRS => 10,
         CURLOPT_TIMEOUT => 30,
         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
         CURLOPT_CUSTOMREQUEST => "POST",
         //CURLOPT_POSTFIELDS => "{\"codProveedor\": \"Contactalos\",\"destinatarios\": [\"".$celular."\"],\"mensaje\": \"".$mensaje."\"}",
         CURLOPT_POSTFIELDS => "{\"codProveedor\": \"SwColombia\",\"destinatarios\": [\"".$celular."\"],\"mensaje\": \"".$mensaje."\"}",
         CURLOPT_HTTPHEADER => array(
        "Authorization: Basic Q09WSURfMTk6ezRFOUM5NjBBLTg3N0YtNEZGRS1BQTVGLTE4MUYwMTg1NTc2Qn0=",
        "Content-Type: application/json"
         ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
         //echo "cURL Error #:" . $err;
        } else {
         //echo $response;
        }

    }

	
	public function enviarCorreo($datos_tramite){
		
		require_once(APPPATH.'libraries/PHPMailer_5.2.4/class.phpmailer.php');
		$mail = new PHPMailer(true);

		try {
				$mail->IsSMTP(); // set mailer to use SMTP
				$mail->Host = "172.16.0.238"; // specif smtp server
				$mail->SMTPSecure= ""; // Used instead of TLS when only POP mail is selected
				$mail->Port = 25; // Used instead of 587 when only POP mail is selected
				$mail->SMTPAuth = false;
				$mail->Username = "cuidatesefeliz@saludcapital.gov.co"; // SMTP username
				$mail->Password = "Colombia2018"; // SMTP password
				$mail->FromName = "Secretaría Distrital de Salud";
				$mail->From = "contactenos@saludcapital.gov.co";
				//$mail->AddAddress($correo_electronico, "CUIDATE"); //replace myname and mypassword to yours
				$mail->AddAddress($datos_tramite['email'], $datos_tramite['email']);
				//$mail->AddReplyTo("acangel@saludcapital.gov.co", "DUES");
				$mail->WordWrap = 50;
				$mail->CharSet = 'UTF-8';
				$mail->AddEmbeddedImage('assets/imgs/logo_pdf_alcaldia.png', 'imagen');
				$mail->AddEmbeddedImage('assets/imgs/logo_pdf_footer.png', 'imagen2');
				$mail->IsHTML(true); // set email format to HTML
				$mail->Subject = 'Secretaría Distrital de Salud - COVID-19';
				
				$html = "
						<table border=\"0\" width=\"80% \" align= \"justify\"><tr align=\"justify\"><td style = \" border: inset 0pt\" >" .
						"<p align=\"justify\"><img src='cid:imagen'/></p>" .
						"</br><h3>Estimado(a): ".$datos_tramite['nombre']."</h3> " .
						"<p>Por medio del presente correo electrónico, la Secretaria Distrital de Salud confirma el registró de tu información para realizar el seguimiento correspondiente</p>" .
						"<p align=\"justify\"><img src='cid:imagen2'/></p></table>";

				$mail->Body = nl2br ($html,false);

				$mail->Send();

		}catch (Exception $e){
			print_r($e->getMessage());
			exit;
		}
	}
    
    public function enviarCorreoAlerta($datos_alerta){
		
		require_once(APPPATH.'libraries/PHPMailer_5.2.4/class.phpmailer.php');
		$mail = new PHPMailer(true);

		try {
				$mail->IsSMTP(); // set mailer to use SMTP
				$mail->Host = "172.16.0.238"; // specif smtp server
				$mail->SMTPSecure= ""; // Used instead of TLS when only POP mail is selected
				$mail->Port = 25; // Used instead of 587 when only POP mail is selected
				$mail->SMTPAuth = false;
				$mail->Username = "cuidatesefeliz@saludcapital.gov.co"; // SMTP username
				$mail->Password = "Colombia2018"; // SMTP password
				$mail->FromName = "Secretaría Distrital de Salud";
				$mail->From = "AlertasCoronavirus@saludcapital.gov.co";
				//$mail->AddAddress($correo_electronico, "CUIDATE"); //replace myname and mypassword to yours
				$mail->AddAddress("AlertasCoronavirus@saludcapital.gov.co", "AlertasCoronavirus@saludcapital.gov.co");
				//$mail->AddReplyTo("acangel@saludcapital.gov.co", "DUES");
				$mail->WordWrap = 50;
				$mail->CharSet = 'UTF-8';
				$mail->AddEmbeddedImage('assets/imgs/logo_pdf_alcaldia.png', 'imagen');
				$mail->AddEmbeddedImage('assets/imgs/logo_pdf_footer.png', 'imagen2');
				$mail->IsHTML(true); // set email format to HTML
				$mail->Subject = 'Alerta Ciudadano - COVID-19';
				                
				$html = "
						<table border=\"0\" width=\"80% \" align= \"justify\"><tr align=\"justify\"><td style = \" border: inset 0pt\" >" .
						"<p align=\"justify\"><img src='cid:imagen'/></p>" .
						"</br><h3>Atención: El siguiente paciente respondio no a las preguntas del formulario de autocuidado individual y colectivo:</h3> " .
						"<p>Identificación: ".$datos_alerta['identificacion']."</p>" .
						"<p>Nombre: ".$datos_alerta['nombre']."</p>" .
						"<p>Email: ".$datos_alerta['correo']."</p>" .
						"<p>Teléfono: ".$datos_alerta['telefono']."</p>" .
						"<p>Dirección: ".$datos_alerta['direccion']."</p>" .
						"<p>EPS: ".$datos_alerta['eps']."</p>" .
						"<p>Fecha nacimiento: ".$datos_alerta['fecha_nacimiento']."</p>" .
						"<p>Pregunta 1: ".$datos_alerta['pregunta1']."</p>" .
						"<p>Pregunta 2: ".$datos_alerta['pregunta2']."</p>" .
						"<p>".$datos_alerta['pregunta3']."</p>" .
						"<p align=\"justify\"><img src='cid:imagen2'/></p></table>";

				$mail->Body = nl2br ($html,false);

				$mail->Send();
		}catch (Exception $e){
			print_r($e->getMessage());
			exit;
		}
	}
    
    public function validarSintoma(){
        
        $id_html = $_POST['id_html'];
        $sintoma = $_POST['id_sintoma'];
        $respuesta = $_POST['respuesta'];
        $edad = $_POST['edad'];
        
        $resultadoSintoma = $this->covid_model->consultarSintoma($sintoma);
        
        if($edad <= 3){
            $id_riesgo = $resultadoSintoma->id_riesgo_curso1;
            $mensaje = $resultadoSintoma->curso1;            
        }else if($edad > 3 && $edad < 24){
            $id_riesgo = $resultadoSintoma->id_riesgo_curso2;
            $mensaje = $resultadoSintoma->curso2;
        }else if($edad >= 24 && $edad < 60){
            $id_riesgo = $resultadoSintoma->id_riesgo_curso3;
            $mensaje = $resultadoSintoma->curso3;
        }else if($edad >= 60 && $edad < 144){
            $id_riesgo = $resultadoSintoma->id_riesgo_curso4;
            $mensaje = $resultadoSintoma->curso4;
        }else if($edad >= 144 && $edad < 780){
            $id_riesgo = $resultadoSintoma->id_riesgo_curso5;
            $mensaje = $resultadoSintoma->curso5;
        }else if($edad >= 780){
            $id_riesgo = $resultadoSintoma->id_riesgo_curso6;
            $mensaje = $resultadoSintoma->curso6;
        }
        
        if($respuesta == 1){
            if($id_riesgo == 1){
                echo '<div class="alert alert-danger" role="alert">
                          '.$mensaje.'
                        </div>';
            }else if($id_riesgo == 2){
                echo '<div class="alert alert-warning" role="alert">
                          '.$mensaje.'
                        </div>';
            }else if($id_riesgo == 3){
                echo '<div class="alert alert-primary" role="alert">
                          '.$mensaje.'
                        </div>';
            }    
        }else{
            echo '';
        }
    }
	
	
	public function excel_individual(){
		
		$listado_per = $this->covid_model->registroIndividual(0);
		
		if(count($listado_per) > 0){
			require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
			$this->excel = new PHPExcel();

			$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setTitle('Registro información');
			//Contador de filas
			$contador = 1;
			// ancho las columnas.
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("L")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("M")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("N")->setWidth(20);
			
			// negrita a los títulos de la cabecera.
			$this->excel->getActiveSheet()->getStyle("A{$contador}:AI{$contador}")->getFont()->setBold(true);
			#$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
			// títulos de la cabecera.
			$this->excel->getActiveSheet()->setCellValue("A{$contador}", 'ID Persona');
			$this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Identificación');
			$this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Nombre');
			$this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Correo');
			$this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Teléfono');
			$this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Dirección');
			$this->excel->getActiveSheet()->setCellValue("G{$contador}", 'EPS');
			$this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Fecha Nacimiento');
			$this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Pregunta 1');
			$this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Pregunta 2');
            $this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Pregunta 3');
            $this->excel->getActiveSheet()->setCellValue("L{$contador}", 'Pregunta 4');
            $this->excel->getActiveSheet()->setCellValue("M{$contador}", 'Pregunta 5');
            $this->excel->getActiveSheet()->setCellValue("N{$contador}", 'Pregunta 6');
            $this->excel->getActiveSheet()->setCellValue("O{$contador}", 'Fecha registro');
			

			foreach($listado_per as $l){
			   $contador++;
			   //Informacion de la consulta.
			   $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_individual);
			   $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->identificacion);
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->nombre);
			   $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->correo);
			   $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->telefono);
			   $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->direccion);
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->eps);
			   $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->fecha_nacimiento);
				if($l->pregunta1 == 1){	$pre1 = "SI";}else if($l->pregunta1 == 0){$pre1 = "NO";}else{$pre1 = $l->pregunta1;}
			   $this->excel->getActiveSheet()->setCellValue("I{$contador}", $pre1);
			   		   
			   if($l->pregunta2 == 1){	$pre2 = "SI";}else if($l->pregunta2 == 0){$pre2 = "NO";}else{$pre2 = $l->pregunta2;}
			   $this->excel->getActiveSheet()->setCellValue("J{$contador}", $pre2);
			   
			   if($l->pregunta3 == 1){  $pre2 = "SI";}else if($l->pregunta3 == 0){$pre2 = "NO";}else{$pre2 = $l->pregunta3;}
               $this->excel->getActiveSheet()->setCellValue("K{$contador}", $pre2);

               if($l->pregunta4 == 1){  $pre2 = "SI";}else if($l->pregunta4 == 0){$pre2 = "NO";}else{$pre2 = $l->pregunta4;}
               $this->excel->getActiveSheet()->setCellValue("L{$contador}", $pre2);

               if($l->pregunta5 == 1){  $pre2 = "SI";}else if($l->pregunta5 == 0){$pre2 = "NO";}else{$pre2 = $l->pregunta5;}
               $this->excel->getActiveSheet()->setCellValue("M{$contador}", $pre2);

               if($l->pregunta6 == 1){  $pre2 = "SI";}else if($l->pregunta6 == 0){$pre2 = "NO";}else{$pre2 = $l->pregunta6;}
               $this->excel->getActiveSheet()->setCellValue("N{$contador}", $pre2);
               $this->excel->getActiveSheet()->setCellValue("O{$contador}", $l->fecha_registro);
			   
			   /*if (isset($l->des_estado))
				   $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->des_estado);
			   else
				   $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->estadoNotif);

			   $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->obs);
			   $this->excel->getActiveSheet()->setCellValue("N{$contador}", $l->pnom_funcionario." ".$l->snom_funcionario." ".$l->pape_funcionario." ".$l->sape_funcionario);
			   $this->excel->getActiveSheet()->setCellValue("O{$contador}", $l->fecha_registro);*/
			}
			//nombre de archivo que se va a generar.
			$archivo = "Registro Covid-19.xls";
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$archivo.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			// salida al navegador con el archivo Excel.
			$objWriter->save('php://output');
		 }/*else{
			echo 'No se encontraron registros';
			exit;
		 }*/
	}

    public function excel_colectivo(){
        
        $listado_per = $this->covid_model->registroColectivo(0);
        
        if(count($listado_per) > 0){
            require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
            $this->excel = new PHPExcel();

            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('Registro información');
            //Contador de filas
            $contador = 1;
            // ancho las columnas.
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(20);
            
                        
            // negrita a los títulos de la cabecera.
            $this->excel->getActiveSheet()->getStyle("A{$contador}:AI{$contador}")->getFont()->setBold(true);
            #$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
            // títulos de la cabecera.
            $this->excel->getActiveSheet()->setCellValue("A{$contador}", 'ID Persona');
            $this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Identificación');
            $this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Nombre');
            $this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Correo');
            $this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Teléfono');
            $this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Dirección');
            $this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Pregunta 1');
            $this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Pregunta 2');
            $this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Pregunta 3');
            $this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Pregunta 4');
            $this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Fecha registro');
            

            foreach($listado_per as $l){
               $contador++;
               //Informacion de la consulta.
               $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_colectivo);
               $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->identificacion);
               $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->nombre);
               $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->correo);
               $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->telefono);
               $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->direccion);
               
                if($l->pregunta1 == 1){ $pre1 = "SI";}else if($l->pregunta1 == 0){$pre1 = "NO";}else{$pre1 = $l->pregunta1;}
               $this->excel->getActiveSheet()->setCellValue("G{$contador}", $pre1);
                       
               if($l->pregunta2 == 1){  $pre2 = "SI";}else if($l->pregunta2 == 0){$pre2 = "NO";}else{$pre2 = $l->pregunta2;}
               $this->excel->getActiveSheet()->setCellValue("H{$contador}", $pre2);
               
               if($l->pregunta3 == 1){  $pre2 = "SI";}else if($l->pregunta3 == 0){$pre2 = "NO";}else{$pre2 = $l->pregunta3;}
               $this->excel->getActiveSheet()->setCellValue("I{$contador}", $pre2);

               if($l->pregunta4 == 1){  $pre2 = "SI";}else if($l->pregunta4 == 0){$pre2 = "NO";}else{$pre2 = $l->pregunta4;}
               $this->excel->getActiveSheet()->setCellValue("J{$contador}", $pre2);

              
               $this->excel->getActiveSheet()->setCellValue("K{$contador}", $l->fecha_registro);
               
               /*if (isset($l->des_estado))
                   $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->des_estado);
               else
                   $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->estadoNotif);

               $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->obs);
               $this->excel->getActiveSheet()->setCellValue("N{$contador}", $l->pnom_funcionario." ".$l->snom_funcionario." ".$l->pape_funcionario." ".$l->sape_funcionario);
               $this->excel->getActiveSheet()->setCellValue("O{$contador}", $l->fecha_registro);*/
            }
            //nombre de archivo que se va a generar.
            $archivo = "Registro Covid-19.xls";
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$archivo.'"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            // salida al navegador con el archivo Excel.
            $objWriter->save('php://output');
         }/*else{
            echo 'No se encontraron registros';
            exit;
         }*/
    }
	
}
