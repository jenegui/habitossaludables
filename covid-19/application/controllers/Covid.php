<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Covid extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    /**
     * [__construct description]
     *
     * @method __construct
     */
    public function __construct()
    {
        // Load the constructer from MY_Controller
        parent::__construct();
        $this->load->database('default');
        $this->load->model('covid_model');
        $this->load->helper('url');
    }

    /**
     * [index description]
     *
     * @method index
     *
     * @return [type] [description]
     */
	public function index()
	{
        //
        $data['js'] = array(base_url('assets/js/covid.js'));
        $data['titulo'] = 'Covid-19';
        $data['contenido'] = "inicio";
        $data['controller']="covid";
		$this->load->view('layout_principal', $data);
	}
    
    public function procesarForm(){
        
        
        
        $data['identificacion'] = $_REQUEST['identificacion'];
        $data['nombre'] = $_REQUEST['nombre'];        
        $data['correo'] = $_REQUEST['correo'];
        $data['correo_conf'] = $_REQUEST['correo_conf'];
        $data['telefono'] = $_REQUEST['telefono'];
        $data['direccion'] = $_REQUEST['direccion'];
        $data['eps'] = $_REQUEST['eps'];
        $data['fecha_nacimiento'] = $_REQUEST['enc_fech_nac'];
        $data['edad'] = $_REQUEST['edad'];
        
        if(isset($_REQUEST['pregunta1'])){
            $data['pregunta1'] = $_REQUEST['pregunta1'];
            
        }else{
			$data['pregunta1'] = NULL;
		}
        
        if(isset($_REQUEST['pregunta2'])){
            $data['pregunta2'] = $_REQUEST['pregunta2'];
        }else{
			$data['pregunta2'] = NULL;
		}
        
        $data['pregunta3'] = '';
        if(isset($_REQUEST['pregunta3'])){
            if(count($_REQUEST['pregunta3']) > 0){
                for($i=0;$i<count($_REQUEST['pregunta3']);$i++){
                    $data['pregunta3'] .= $_REQUEST['pregunta3'][$i].",";
                }
            }
            //$data['pregunta3'] = $_REQUEST['pregunta3'];
        }else{
			$data['pregunta3'] = NULL;
		}
        
        if(isset($_REQUEST['sintoma1'])){$data['sintoma1'] = $_REQUEST['sintoma1'];}
        if(isset($_REQUEST['sintoma2'])){$data['sintoma2'] = $_REQUEST['sintoma2'];}
        if(isset($_REQUEST['sintoma3'])){$data['sintoma3'] = $_REQUEST['sintoma3'];}
        if(isset($_REQUEST['sintoma4'])){$data['sintoma4'] = $_REQUEST['sintoma4'];}
        if(isset($_REQUEST['sintoma5'])){$data['sintoma5'] = $_REQUEST['sintoma5'];}
        if(isset($_REQUEST['sintoma6'])){$data['sintoma6'] = $_REQUEST['sintoma6'];}
        if(isset($_REQUEST['sintoma7'])){$data['sintoma7'] = $_REQUEST['sintoma7'];}
        if(isset($_REQUEST['sintoma8'])){$data['sintoma8'] = $_REQUEST['sintoma8'];}
        if(isset($_REQUEST['sintoma9'])){$data['sintoma9'] = $_REQUEST['sintoma9'];}
        if(isset($_REQUEST['sintoma10'])){$data['sintoma10'] = $_REQUEST['sintoma10'];}
        if(isset($_REQUEST['sintoma11'])){$data['sintoma11'] = $_REQUEST['sintoma11'];}
        if(isset($_REQUEST['sintoma12'])){$data['sintoma12'] = $_REQUEST['sintoma12'];}
        if(isset($_REQUEST['sintoma13'])){$data['sintoma13'] = $_REQUEST['sintoma13'];}
        if(isset($_REQUEST['sintoma14'])){$data['sintoma14'] = $_REQUEST['sintoma14'];}
        if(isset($_REQUEST['sintoma15'])){$data['sintoma15'] = $_REQUEST['sintoma15'];}
        if(isset($_REQUEST['sintoma16'])){$data['sintoma16'] = $_REQUEST['sintoma16'];}
        if(isset($_REQUEST['sintoma17'])){$data['sintoma17'] = $_REQUEST['sintoma17'];}
        if(isset($_REQUEST['sintoma18'])){$data['sintoma18'] = $_REQUEST['sintoma18'];}
        if(isset($_REQUEST['sintoma19'])){$data['sintoma19'] = $_REQUEST['sintoma19'];}
        if(isset($_REQUEST['sintoma20'])){$data['sintoma20'] = $_REQUEST['sintoma20'];}
        if(isset($_REQUEST['sintoma21'])){$data['sintoma21'] = $_REQUEST['sintoma21'];}
        if(isset($_REQUEST['sintoma22'])){$data['sintoma22'] = $_REQUEST['sintoma22'];}
        if(isset($_REQUEST['sintoma23'])){$data['sintoma23'] = $_REQUEST['sintoma23'];}
        if(isset($_REQUEST['sintoma24'])){$data['sintoma24'] = $_REQUEST['sintoma24'];}
		$data['fecha_registro'] = date('Y-m-d H:i:s');
        
        $resultado = $this->covid_model->registrarPersona($data);
        
        if($resultado){
			
			$datos_tramite['nombre'] = $_REQUEST['nombre'];
			$datos_tramite['email'] = $_REQUEST['correo'];
			$this->enviarCorreo($datos_tramite);
			
            if($_REQUEST['pregunta1'] == 1 && $_REQUEST['pregunta2'] == 1 && (count($_REQUEST['pregunta3']) >= 1)){
                $datos_alerta['pregunta1'] = 'SI';
                $datos_alerta['pregunta2'] = 'SI';
                $datos_alerta['identificacion'] = $_REQUEST['identificacion'];
                $datos_alerta['nombre'] = $_REQUEST['nombre'];        
                $datos_alerta['correo'] = $_REQUEST['correo'];
                $datos_alerta['correo_conf'] = $_REQUEST['correo_conf'];
                $datos_alerta['telefono'] = $_REQUEST['telefono'];
                $datos_alerta['direccion'] = $_REQUEST['direccion'];
                $datos_alerta['eps'] = $_REQUEST['eps'];
                $datos_alerta['fecha_nacimiento'] = $_REQUEST['enc_fech_nac'];
                $datos_alerta['edad'] = $_REQUEST['edad'];
                
                $datos_alerta['pregunta3'] = "Sintomas: ";
                if(count($_REQUEST['pregunta3']) > 0){
                    for($i=0;$i<count($_REQUEST['pregunta3']);$i++){
                        if($_REQUEST['pregunta3'][$i] == 1){
                            $datos_alerta['pregunta3'] .= "Fiebre cuantificada mayor o igual a 38°C, ";
                        }else if($_REQUEST['pregunta3'][$i] == 2){
                            $datos_alerta['pregunta3'] .= "Tos, ";
                        }else if($_REQUEST['pregunta3'][$i] == 3){
                            $datos_alerta['pregunta3'] .= "Dificultad para respirar, ";
                        }else if($_REQUEST['pregunta3'][$i] == 4){
                            $datos_alerta['pregunta3'] .= "Dolor de garganta (Odinofagia), ";
                        }else if($_REQUEST['pregunta3'][$i] == 5){
                            $datos_alerta['pregunta3'] .= "Fatiga/decaimiento o debilidad, ";
                        }else if($_REQUEST['pregunta3'][$i] == 6){
                            $datos_alerta['pregunta3'] .= "Otros (comportamiento o actitud, Alimentacion), ";
                        }
                        
                    }
                }
                
                $this->enviarCorreoAlerta($datos_alerta);
            }
            
            $this->session->set_flashdata('exito', 'Se realizó el registro de la información');
            redirect(base_url('covid/'), 'refresh');
            exit;    
        }else{
            $this->session->set_flashdata('error', 'No fue posible realizar el registro de información');
            redirect(base_url('covid/'), 'refresh');
            exit; 
        }
        
        
    }
	
	public function enviarCorreo($datos_tramite){
		
		require_once(APPPATH.'libraries/PHPMailer_5.2.4/class.phpmailer.php');
		$mail = new PHPMailer(true);

		try {
				$mail->IsSMTP(); // set mailer to use SMTP
				$mail->Host = "172.16.0.238"; // specif smtp server
				$mail->SMTPSecure= ""; // Used instead of TLS when only POP mail is selected
				$mail->Port = 25; // Used instead of 587 when only POP mail is selected
				$mail->SMTPAuth = false;
				$mail->Username = "cuidatesefeliz@saludcapital.gov.co"; // SMTP username
				$mail->Password = "Colombia2018"; // SMTP password
				$mail->FromName = "Secretaría Distrital de Salud";
				$mail->From = "contactenos@saludcapital.gov.co";
				//$mail->AddAddress($correo_electronico, "CUIDATE"); //replace myname and mypassword to yours
				$mail->AddAddress($datos_tramite['email'], $datos_tramite['email']);
				//$mail->AddReplyTo("acangel@saludcapital.gov.co", "DUES");
				$mail->WordWrap = 50;
				$mail->CharSet = 'UTF-8';
				$mail->AddEmbeddedImage('assets/imgs/logo_pdf_alcaldia.png', 'imagen');
				$mail->AddEmbeddedImage('assets/imgs/logo_pdf_footer.png', 'imagen2');
				$mail->IsHTML(true); // set email format to HTML
				$mail->Subject = 'Secretaría Distrital de Salud - COVID-19';
				
				$html = "
						<table border=\"0\" width=\"80% \" align= \"justify\"><tr align=\"justify\"><td style = \" border: inset 0pt\" >" .
						"<p align=\"justify\"><img src='cid:imagen'/></p>" .
						"</br><h3>Estimado(a): ".$datos_tramite['nombre']."</h3> " .
						"<p>Por medio del presente correo electrónico, la Secretaria Distrital de Salud confirma el registró de tu información para realizar el seguimiento correspondiente</p>" .
						"<p align=\"justify\"><img src='cid:imagen2'/></p></table>";

				$mail->Body = nl2br ($html,false);

				$mail->Send();

		}catch (Exception $e){
			print_r($e->getMessage());
			exit;
		}
	}
    
    public function enviarCorreoAlerta($datos_alerta){
		
		require_once(APPPATH.'libraries/PHPMailer_5.2.4/class.phpmailer.php');
		$mail = new PHPMailer(true);

		try {
				$mail->IsSMTP(); // set mailer to use SMTP
				$mail->Host = "172.16.0.238"; // specif smtp server
				$mail->SMTPSecure= ""; // Used instead of TLS when only POP mail is selected
				$mail->Port = 25; // Used instead of 587 when only POP mail is selected
				$mail->SMTPAuth = false;
				$mail->Username = "cuidatesefeliz@saludcapital.gov.co"; // SMTP username
				$mail->Password = "Colombia2018"; // SMTP password
				$mail->FromName = "Secretaría Distrital de Salud";
				$mail->From = "AlertasCoronavirus@saludcapital.gov.co";
				//$mail->AddAddress($correo_electronico, "CUIDATE"); //replace myname and mypassword to yours
				$mail->AddAddress("AlertasCoronavirus@saludcapital.gov.co", "AlertasCoronavirus@saludcapital.gov.co");
				//$mail->AddReplyTo("acangel@saludcapital.gov.co", "DUES");
				$mail->WordWrap = 50;
				$mail->CharSet = 'UTF-8';
				$mail->AddEmbeddedImage('assets/imgs/logo_pdf_alcaldia.png', 'imagen');
				$mail->AddEmbeddedImage('assets/imgs/logo_pdf_footer.png', 'imagen2');
				$mail->IsHTML(true); // set email format to HTML
				$mail->Subject = 'Alerta Ciudadano - COVID-19';
				                
				$html = "
						<table border=\"0\" width=\"80% \" align= \"justify\"><tr align=\"justify\"><td style = \" border: inset 0pt\" >" .
						"<p align=\"justify\"><img src='cid:imagen'/></p>" .
						"</br><h3>Atención: El siguiente paciente respondio positivamente las dos preguntas del formulario de autoevaluación:</h3> " .
						"<p>Identificación: ".$datos_alerta['identificacion']."</p>" .
						"<p>Nombre: ".$datos_alerta['nombre']."</p>" .
						"<p>Email: ".$datos_alerta['correo']."</p>" .
						"<p>Teléfono: ".$datos_alerta['telefono']."</p>" .
						"<p>Dirección: ".$datos_alerta['direccion']."</p>" .
						"<p>EPS: ".$datos_alerta['eps']."</p>" .
						"<p>Fecha nacimiento: ".$datos_alerta['fecha_nacimiento']."</p>" .
						"<p>Pregunta 1: ".$datos_alerta['pregunta1']."</p>" .
						"<p>Pregunta 2: ".$datos_alerta['pregunta2']."</p>" .
						"<p>".$datos_alerta['pregunta3']."</p>" .
						"<p align=\"justify\"><img src='cid:imagen2'/></p></table>";

				$mail->Body = nl2br ($html,false);

				$mail->Send();
		}catch (Exception $e){
			print_r($e->getMessage());
			exit;
		}
	}
    
    public function validarSintoma(){
        
        $id_html = $_POST['id_html'];
        $sintoma = $_POST['id_sintoma'];
        $respuesta = $_POST['respuesta'];
        $edad = $_POST['edad'];
        
        $resultadoSintoma = $this->covid_model->consultarSintoma($sintoma);
        
        if($edad <= 3){
            $id_riesgo = $resultadoSintoma->id_riesgo_curso1;
            $mensaje = $resultadoSintoma->curso1;            
        }else if($edad > 3 && $edad < 24){
            $id_riesgo = $resultadoSintoma->id_riesgo_curso2;
            $mensaje = $resultadoSintoma->curso2;
        }else if($edad >= 24 && $edad < 60){
            $id_riesgo = $resultadoSintoma->id_riesgo_curso3;
            $mensaje = $resultadoSintoma->curso3;
        }else if($edad >= 60 && $edad < 144){
            $id_riesgo = $resultadoSintoma->id_riesgo_curso4;
            $mensaje = $resultadoSintoma->curso4;
        }else if($edad >= 144 && $edad < 780){
            $id_riesgo = $resultadoSintoma->id_riesgo_curso5;
            $mensaje = $resultadoSintoma->curso5;
        }else if($edad >= 780){
            $id_riesgo = $resultadoSintoma->id_riesgo_curso6;
            $mensaje = $resultadoSintoma->curso6;
        }
        
        if($respuesta == 1){
            if($id_riesgo == 1){
                echo '<div class="alert alert-danger" role="alert">
                          '.$mensaje.'
                        </div>';
            }else if($id_riesgo == 2){
                echo '<div class="alert alert-warning" role="alert">
                          '.$mensaje.'
                        </div>';
            }else if($id_riesgo == 3){
                echo '<div class="alert alert-primary" role="alert">
                          '.$mensaje.'
                        </div>';
            }    
        }else{
            echo '';
        }
    }
	
	
	public function generar_excel(){
		
		$listado_per = $this->covid_model->registroPersonasConsulta();
		
		if(count($listado_per) > 0){
			require_once APPPATH.'libraries/PHPExcel/PHPExcel.php';
			$this->excel = new PHPExcel();

			$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setTitle('Registro información');
			//Contador de filas
			$contador = 1;
			// ancho las columnas.
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("D")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("E")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("F")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("G")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("H")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("I")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("J")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("K")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("L")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("M")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("N")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("O")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("P")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("Q")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("R")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("S")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("T")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("U")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("V")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("W")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("X")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("Y")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("Z")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("AA")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("AB")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("AC")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("AD")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("AE")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("AF")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("AG")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("AH")->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension("AI")->setWidth(20);
			// negrita a los títulos de la cabecera.
			$this->excel->getActiveSheet()->getStyle("A{$contador}:AI{$contador}")->getFont()->setBold(true);
			#$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
			// títulos de la cabecera.
			$this->excel->getActiveSheet()->setCellValue("A{$contador}", 'ID Persona');
			$this->excel->getActiveSheet()->setCellValue("B{$contador}", 'Identificación');
			$this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Nombre');
			$this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Correo');
			$this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Teléfono');
			$this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Dirección');
			$this->excel->getActiveSheet()->setCellValue("G{$contador}", 'EPS');
			$this->excel->getActiveSheet()->setCellValue("H{$contador}", 'Fecha Nacimiento');
			$this->excel->getActiveSheet()->setCellValue("I{$contador}", 'Pregunta 1');
			$this->excel->getActiveSheet()->setCellValue("J{$contador}", 'Pregunta 2');
			$this->excel->getActiveSheet()->setCellValue("K{$contador}", 'Sintomas');
			$this->excel->getActiveSheet()->setCellValue("L{$contador}", 'Fiebre - SI, no mas de 3 dias ');
			$this->excel->getActiveSheet()->setCellValue("M{$contador}", 'Fiebre - Alta mayor de 39 grados,dificil de controlar o mas. ');
			$this->excel->getActiveSheet()->setCellValue("N{$contador}", 'Fiebre - NO');
			$this->excel->getActiveSheet()->setCellValue("O{$contador}", 'Tos - Tiene  expectoración con pintas de sangre o de color café oscuro');
			$this->excel->getActiveSheet()->setCellValue("P{$contador}", 'Tos - Presenta tos Ocasional');
			$this->excel->getActiveSheet()->setCellValue("Q{$contador}", 'Tos - Tos persistente que lo pone rojo o lo hace vomitar');
			$this->excel->getActiveSheet()->setCellValue("R{$contador}", 'Tos - No tiene tos');
			$this->excel->getActiveSheet()->setCellValue("S{$contador}", 'Presenta Sensación ahogo y/o Pausa respiratoria');
			$this->excel->getActiveSheet()->setCellValue("T{$contador}", 'Tiene respiración rápida/ retracciones de costillas');
			$this->excel->getActiveSheet()->setCellValue("U{$contador}", 'Se le escuchan silbidos y/o ruidos extraños al respirar?');
			$this->excel->getActiveSheet()->setCellValue("V{$contador}", 'No tiene dificultad para respirar');
			$this->excel->getActiveSheet()->setCellValue("W{$contador}", 'Tiene Enfermedad Cardíaca o pulmonar congénita');
			$this->excel->getActiveSheet()->setCellValue("X{$contador}", 'Fue Prematuro o de bajo peso al nacer');
			$this->excel->getActiveSheet()->setCellValue("Y{$contador}", 'Tiene Asma, EPOC, Enfermedad hepática y/o Cáncer');
			$this->excel->getActiveSheet()->setCellValue("Z{$contador}", 'Anemia de células falciformes');
			$this->excel->getActiveSheet()->setCellValue("AA{$contador}", 'Ha tenido Convulsiones y/o Epilepsia');
			$this->excel->getActiveSheet()->setCellValue("AB{$contador}", 'Ninguno de los anteriores');
			$this->excel->getActiveSheet()->setCellValue("AC{$contador}", 'Esta somnoliento o muy dormido');
			$this->excel->getActiveSheet()->setCellValue("AD{$contador}", 'Irritable');
			$this->excel->getActiveSheet()->setCellValue("AE{$contador}", 'Menos activo de lo normal (decaimiento)');
			$this->excel->getActiveSheet()->setCellValue("AF{$contador}", 'Normal');
			$this->excel->getActiveSheet()->setCellValue("AG{$contador}", 'No come nada o vomita todo');
			$this->excel->getActiveSheet()->setCellValue("AH{$contador}", 'Come menos o vomita esporadicamente');
			$this->excel->getActiveSheet()->setCellValue("AI{$contador}", 'Toma liquidos / se alimenta bien');
            $this->excel->getActiveSheet()->setCellValue("AJ{$contador}", 'Fecha registro');

			foreach($listado_per as $l){
			   $contador++;
			   //Informacion de la consulta.
			   $this->excel->getActiveSheet()->setCellValue("A{$contador}", $l->id_persona);
			   $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l->identificacion);
			   $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l->nombre);
			   $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l->correo);
			   $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l->telefono);
			   $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l->direccion);
			   $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l->eps);
			   $this->excel->getActiveSheet()->setCellValue("H{$contador}", $l->fecha_nacimiento);
				if($l->pregunta1 == 1){	$pre1 = "SI";}else if($l->pregunta1 == 0){$pre1 = "NO";}else{$pre1 = $l->pregunta1;}
			   $this->excel->getActiveSheet()->setCellValue("I{$contador}", $pre1);
			   		   
			   if($l->pregunta2 == 1){	$pre2 = "SI";}else if($l->pregunta2 == 0){$pre2 = "NO";}else{$pre2 = $l->pregunta1;}
			   $this->excel->getActiveSheet()->setCellValue("J{$contador}", $pre2);
			   
			   if(count($l->pregunta3) != ''){
					$sint = explode(",", $l->pregunta3);
					$sintomasText = '';
                    for($i=0;$i<count($sint);$i++){
                        if($sint[$i] == 1){
                            $sintomasText .= "Fiebre cuantificada mayor o igual a 38°C, ";
                        }else if($sint[$i] == 2){
                            $sintomasText .= "Tos, ";
                        }else if($sint[$i] == 3){
                            $sintomasText .= "Dificultad para respirar, ";
                        }else if($sint[$i] == 4){
                            $sintomasText .= "Dolor de garganta (Odinofagia), ";
                        }else if($sint[$i] == 5){
                            $sintomasText .= "Fatiga/decaimiento o debilidad, ";
                        }else if($sint[$i] == 6){
                            $sintomasText .= "Otros (comportamiento o actitud, Alimentacion), ";
                        }
                        
                    }
                }else{
					$sintomasText = '';
				}
				$this->excel->getActiveSheet()->setCellValue("K{$contador}", $sintomasText);
				
				if($l->sintoma1 == 1){	$sin1 = "SI";}else if($l->sintoma1 == 0){$sin1 = "NO";}else{$sin1 = $l->sintoma1;}
				$this->excel->getActiveSheet()->setCellValue("L{$contador}", $sin1);
				if($l->sintoma2 == 1){	$sin2 = "SI";}else if($l->sintoma2 == 0){$sin2 = "NO";}else{$sin2 = $l->sintoma2;}
				$this->excel->getActiveSheet()->setCellValue("M{$contador}", $sin2);
				if($l->sintoma3 == 1){	$sin3 = "SI";}else if($l->sintoma3 == 0){$sin3 = "NO";}else{$sin3 = $l->sintoma3;}
				$this->excel->getActiveSheet()->setCellValue("N{$contador}", $sin3);
				if($l->sintoma4 == 1){	$sin4 = "SI";}else if($l->sintoma4 == 0){$sin4 = "NO";}else{$sin4 = $l->sintoma4;}
				$this->excel->getActiveSheet()->setCellValue("O{$contador}", $sin4);
				if($l->sintoma5 == 1){	$sin5 = "SI";}else if($l->sintoma5 == 0){$sin5 = "NO";}else{$sin5 = $l->sintoma5;}
				$this->excel->getActiveSheet()->setCellValue("P{$contador}", $sin5);
				if($l->sintoma6 == 1){	$sin6 = "SI";}else if($l->sintoma6 == 0){$sin6 = "NO";}else{$sin6 = $l->sintoma6;}
				$this->excel->getActiveSheet()->setCellValue("Q{$contador}", $sin6);
				if($l->sintoma7 == 1){	$sin7 = "SI";}else if($l->sintoma7 == 0){$sin7 = "NO";}else{$sin7 = $l->sintoma7;}
				$this->excel->getActiveSheet()->setCellValue("R{$contador}", $sin7);
				if($l->sintoma8 == 1){	$sin8 = "SI";}else if($l->sintoma8 == 0){$sin8 = "NO";}else{$sin8 = $l->sintoma8;}
				$this->excel->getActiveSheet()->setCellValue("S{$contador}", $sin8);
				if($l->sintoma9 == 1){	$sin9 = "SI";}else if($l->sintoma9 == 0){$sin9 = "NO";}else{$sin9 = $l->sintoma9;}
				$this->excel->getActiveSheet()->setCellValue("T{$contador}", $sin9);
				if($l->sintoma10 == 1){	$sin10 = "SI";}else if($l->sintoma10 == 0){$sin10 = "NO";}else{$sin10 = $l->sintoma10;}
				$this->excel->getActiveSheet()->setCellValue("U{$contador}", $sin10);
				if($l->sintoma11 == 1){	$sin11 = "SI";}else if($l->sintoma11 == 0){$sin11 = "NO";}else{$sin11 = $l->sintoma11;}
				$this->excel->getActiveSheet()->setCellValue("V{$contador}", $sin11);
				if($l->sintoma12 == 1){	$sin12 = "SI";}else if($l->sintoma12 == 0){$sin12 = "NO";}else{$sin12 = $l->sintoma12;}
				$this->excel->getActiveSheet()->setCellValue("W{$contador}", $sin12);
				if($l->sintoma13 == 1){	$sin13 = "SI";}else if($l->sintoma13 == 0){$sin13 = "NO";}else{$sin13 = $l->sintoma13;}
				$this->excel->getActiveSheet()->setCellValue("X{$contador}", $sin13);
				if($l->sintoma14 == 1){	$sin14 = "SI";}else if($l->sintoma14 == 0){$sin14 = "NO";}else{$sin14 = $l->sintoma14;}
				$this->excel->getActiveSheet()->setCellValue("Y{$contador}", $sin14);
				if($l->sintoma15 == 1){	$sin15 = "SI";}else if($l->sintoma15 == 0){$sin15 = "NO";}else{$sin15 = $l->sintoma15;}
				$this->excel->getActiveSheet()->setCellValue("Z{$contador}", $sin15);
				if($l->sintoma16 == 1){	$sin16 = "SI";}else if($l->sintoma16 == 0){$sin16 = "NO";}else{$sin16 = $l->sintoma16;}
				$this->excel->getActiveSheet()->setCellValue("AA{$contador}", $sin16);
				if($l->sintoma17 == 1){	$sin17 = "SI";}else if($l->sintoma17 == 0){$sin17 = "NO";}else{$sin17 = $l->sintoma17;}
				$this->excel->getActiveSheet()->setCellValue("AB{$contador}", $sin17);
				if($l->sintoma18 == 1){	$sin18 = "SI";}else if($l->sintoma18 == 0){$sin18 = "NO";}else{$sin18 = $l->sintoma18;}
				$this->excel->getActiveSheet()->setCellValue("AC{$contador}", $sin18);
				if($l->sintoma19 == 1){	$sin19 = "SI";}else if($l->sintoma19 == 0){$sin19 = "NO";}else{$sin19 = $l->sintoma19;}
				$this->excel->getActiveSheet()->setCellValue("AD{$contador}", $sin19);
				if($l->sintoma20 == 1){	$sin20 = "SI";}else if($l->sintoma20 == 0){$sin20 = "NO";}else{$sin20 = $l->sintoma20;}
				$this->excel->getActiveSheet()->setCellValue("AE{$contador}", $sin20);
				if($l->sintoma21 == 1){	$sin21 = "SI";}else if($l->sintoma21 == 0){$sin21 = "NO";}else{$sin21 = $l->sintoma21;}
				$this->excel->getActiveSheet()->setCellValue("AF{$contador}", $sin21);
				if($l->sintoma22 == 1){	$sin22 = "SI";}else if($l->sintoma22 == 0){$sin22 = "NO";}else{$sin22 = $l->sintoma22;}
				$this->excel->getActiveSheet()->setCellValue("AG{$contador}", $sin22);
				if($l->sintoma23 == 1){	$sin23 = "SI";}else if($l->sintoma23 == 0){$sin23 = "NO";}else{$sin23 = $l->sintoma23;}
				$this->excel->getActiveSheet()->setCellValue("AH{$contador}", $sin23);
				if($l->sintoma24 == 1){	$sin24 = "SI";}else if($l->sintoma24 == 0){$sin24 = "NO";}else{$sin24 = $l->sintoma24;}
				$this->excel->getActiveSheet()->setCellValue("AI{$contador}", $sin24);

                $this->excel->getActiveSheet()->setCellValue("AJ{$contador}", $l->fecha_registro);
				
			   
			   /*if (isset($l->des_estado))
				   $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->des_estado);
			   else
				   $this->excel->getActiveSheet()->setCellValue("L{$contador}", $l->estadoNotif);

			   $this->excel->getActiveSheet()->setCellValue("M{$contador}", $l->obs);
			   $this->excel->getActiveSheet()->setCellValue("N{$contador}", $l->pnom_funcionario." ".$l->snom_funcionario." ".$l->pape_funcionario." ".$l->sape_funcionario);
			   $this->excel->getActiveSheet()->setCellValue("O{$contador}", $l->fecha_registro);*/
			}
			//nombre de archivo que se va a generar.
			$archivo = "Registro Covid-19.xls";
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$archivo.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			// salida al navegador con el archivo Excel.
			$objWriter->save('php://output');
		 }/*else{
			echo 'No se encontraron registros';
			exit;
		 }*/
	}
	
}
