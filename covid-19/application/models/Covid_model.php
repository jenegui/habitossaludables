<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 */
class Covid_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }



    public function tramites_pendientes(){
        $cadena_sql = " SELECT id_titulo, RT.id_persona, RT.fecha_tramite, RT.tipo_titulo, RT.institucion_educativa, RT.profesion, RT.fecha_term, RT.tarjeta, RT.diploma, RT.acta,
        RT.libro, RT.folio, RT.anio, RT.cod_universidad, RT.fecha_term_ext, RT.resolucion, RT.fecha_resolucion, RT.entidad, RT.titulo_equivalente,
        RT.pdf_documento, RT.pdf_titulo, RT.pdf_acta, RT.pdf_tarjeta, RT.pdf_resolucion, RT.estado, RT.fecha_editado, RT.fecha_reposicion, ET.descripcion descEstado, TI.Descripcion descTipoIden ,PE.*
        FROM registro_titulo RT
        JOIN persona PE ON PE.id_persona = RT.id_persona
        JOIN pr_estado_tramite ET ON ET.id_estado = RT.estado
        JOIN pr_tipoidentificacion TI ON TI.IdTipoIdentificacion = PE.tipo_identificacion
        WHERE estado IN (3,6,10,18) ORDER BY RT.fecha_tramite ASC";

        $query = $this->db->query($cadena_sql);
        $result = $query->result();
        return $result;
    }

   
    public function actualizarEstado($datos) {

        $data = array(
            'estado' => $datos['estado']
        );
        $this->db->where('id_titulo', $datos['id_titulo']);
        return $this->db->update('registro_titulo', $data);
    }
    
    public function registrarPersona($param) {
        $this->db->trans_start();
        $this->db->insert('persona', $param);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }

    public function registrarIndividual($param) {
        $this->db->trans_start();
        $this->db->insert('individual', $param);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }

    public function registrarColectivo($param) {
        $this->db->trans_start();
        $this->db->insert('colectivo', $param);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return  $insert_id;
    }
    
    public function preguntas_ira($id_sintoma){
        $cadena_sql = " SELECT * FROM ira_sintomas WHERE id_sintoma_principal IN (".$id_sintoma.")";

        $query = $this->db->query($cadena_sql);
        $result = $query->result();
        return $result;
    }
    
    public function consultarSintoma($id_sintoma){
        $cadena_sql = " SELECT * FROM ira_sintomas WHERE id_sintoma = ".$id_sintoma."";

        $query = $this->db->query($cadena_sql);
        $result = $query->row();
        return $result;
    }
	
    public function registroPersonasConsulta(){
        $cadena_sql = " SELECT * FROM persona ";

        $query = $this->db->query($cadena_sql);
        $result = $query->result();
        return $result;
    }

    public function registroIndividual($cedula){
        $cadena_sql = " SELECT * FROM individual ";
        if($cedula!=0){
            $cadena_sql .= "WHERE identificacion = ".$cedula." ";
        }
        $query = $this->db->query($cadena_sql);
        $result = $query->result();
        return $result;
    }

    public function registroColectivo($cedula){
        $cadena_sql = " SELECT * FROM colectivo ";
        if($cedula!=0){
            $cadena_sql .= "WHERE identificacion = ".$cedula." ";
        }
        $query = $this->db->query($cadena_sql);
        $result = $query->result();
        return $result;
    }

}
